import heapq


def solution(operations):
	answer = []
	heap = []

	for operation in operations:
		opcode, num = operation.split(' ')

		# push
		if opcode == 'I':
			heapq.heappush(heap, int(num))
		# pop
		elif heap:
			if num == '1':
				heap.remove(max(heap))  # pop max : O(n)
			else:
				heapq.heappop(heap)	 # pop min : O(1)

	return [max(heap), min(heap)] if heap else [0, 0]


operations = ["I -45", "I 653", "D 1", "I -642", "I 45", "I 97", "D 1", "D -1", "I 333"]
print(solution(operations))
