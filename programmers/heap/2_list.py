# heap을 사용한 방법보다 최대 100배 이상 빠름
def solution(jobs):
	answer = 0
	start = 0  # 현재까지 진행된 작업 시간
	length = len(jobs)

	jobs = sorted(jobs, key=lambda x: x[1])  # 소요시간 우선 정렬

	while jobs:
		for i in range(len(jobs)):
			if jobs[i][0] <= start:
				start += jobs[i][1]
				answer += start - jobs[i][0]
				jobs.pop(i)
				break
			# 해당시점에 아직 작업이 들어오지 않았으면 시간 증가
			if i == len(jobs) - 1:
				start += 1

	return answer // length


jobs = [[0, 3], [1, 9], [2, 6]]
jobs = [[0, 2], [1, 3], [2, 1]]
print(solution(jobs))