import heapq
from collections import deque


def solution(jobs):
	# 나중에 heap 우선순위 순서를 위해 처리시간이 앞에 오도록 설정
	# 도착시각으로 먼저 오름차순 정렬 후 처리시간으로 오름차순 정렬
	length = len(jobs)
	jobs = [(dur, arr) for arr, dur in jobs]
	jobs = deque(sorted(jobs, key=lambda x: (x[1], x[0])))

	# 최소 도착시각을 root(0번 인덱스)로 가지는 min heap
	# 최소 도착시각을 가진 노드가 여러개면 최소 처리시간을 기준으로 사용
	# heap 우선순위 순서를 설정하기 위해 위에서 jobs의 0번, 1번 값 순서 바꾼 것임
	# max heap 쓰려면 마이너스 붙이기
	q = []

	time = 0
	total_response_time = 0

	while jobs or q:

		if len(q) == 0:
			heapq.heappush(q, jobs.popleft())

		dur, arr = heapq.heappop(q)
		time = max(time, arr) + dur
		total_response_time += time - arr

		while jobs and jobs[0][1] <= time:
			heapq.heappush(q, jobs.popleft())

	return total_response_time // length


jobs = [[0, 2], [1, 3], [2, 1]]
jobs = [[0, 3], [1, 9], [2, 6], [0, 2], [1, 6]]
print(solution(jobs))