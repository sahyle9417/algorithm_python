import heapq

def solution(scoville, K):
	# list를 heap으로 변경
	heapq.heapify(scoville)
	ret = 0

	# min 값이 K보다 작은 동안 반복
	while scoville[0] < K:
		try:
			min_1st = heapq.heappop(scoville)
			min_2nd = heapq.heappop(scoville)
			heapq.heappush(scoville, min_1st + min_2nd * 2)
		except IndexError:
			return -1
		ret += 1

	return ret

scoville = [1, 2, 3, 9, 10, 12]
K = 7
print(solution(scoville, K))
