from heapq import heappush, heappop


def solution(arguments):
	max_heap = []
	min_heap = []
	for arg in arguments:
		if arg == "D 1":
			if max_heap:
				heappop(max_heap)
				# 최대힙, 최소힙에 각각 i, j번의 삭제명령이 있었다면
				# -max_heap[0]는 i번째 큰 수이고 현재 힙 내에서는 최대값
				# min_heap[0]는 j번째 작은 수이고 현재 힙 내에서는 최소값
				# 현재 힙 내에서 최소값이 최대값보다 크다면 힙은 비어있는 것임
				if max_heap == [] or -max_heap[0] < min_heap[0]:
					min_heap = []
					max_heap = []
		elif arg == "D -1":
			if min_heap:
				heappop(min_heap)
				if min_heap == [] or -max_heap[0] < min_heap[0]:
					max_heap = []
					min_heap = []
		else:
			num = int(arg[2:])
			heappush(max_heap, -num)
			heappush(min_heap, num)

	return [-heappop(max_heap), heappop(min_heap)] if min_heap else [0, 0]
