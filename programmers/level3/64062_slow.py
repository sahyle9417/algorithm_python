# 시간 초과
from collections import defaultdict


def solution(stones, k):
	stone_to_pos = defaultdict(list)
	for pos, stone in enumerate(stones):
		stone_to_pos[stone].append(pos)

	missing_pos = []

	for stone in sorted(stone_to_pos):
		missing_pos.extend(stone_to_pos[stone])
		missing_pos = sorted(missing_pos)

		if not can_jump(missing_pos, k):
			return stone

	return stone


def can_jump(missing_pos, jump):
	if len(missing_pos) == 0:
		return True
	if jump == 1:
		return False

	missing_len = 1
	pos = missing_pos[0]

	for next_pos in missing_pos[1:]:
		if next_pos == pos + 1:
			missing_len += 1
			if missing_len == jump:
				return False
		else:  # next_pos != pos + 1:
			missing_len = 1
		pos = next_pos

	return True



stones = [2, 4, 5, 3, 2, 1, 4, 2, 5, 1]
k = 3
print(solution(stones, k))

