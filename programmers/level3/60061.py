def solution(n, build_frame):
	status = set()

	for x, y, shape, add in build_frame:
		# 추가
		if add:
			if is_valid(status | {(x, y, shape)}):
				status.add((x, y, shape))
		# 삭제
		else:
			if is_valid(status - {(x, y, shape)}):
				status.remove((x, y, shape))

	status = sorted(status, key=lambda x: (x[0], x[1], x[2]))
	status = list(map(list, status))
	return status


def is_valid(status):
	for x, y, shape in status:
		# 기둥
		if shape == 0:
			# 바닥 위에 있거나 보의 한쪽 끝 부분 위에 있거나, 또는 다른 기둥 위에 있어야 합니다.
			if y == 0:
				continue
			elif (x, y, 1) in status:
				continue
			elif (x - 1, y, 1) in status:
				continue
			elif (x, y - 1, 0) in status:
				continue
			else:
				return False
		# 보
		else:
			# 한쪽 끝 부분이 기둥 위에 있거나, 또는 양쪽 끝 부분이 다른 보와 동시에 연결되어 있어야 합니다.
			if (x, y - 1, 0) in status:
				continue
			elif (x + 1, y - 1, 0) in status:
				continue
			elif (x - 1, y, 1) in status and (x + 1, y, 1) in status:
				continue
			else:
				return False
	return True


"""
n = 5
build_frame = [
	[0, 0, 0, 1],
	[2, 0, 0, 1],
	[4, 0, 0, 1],
	[0, 1, 1, 1],
	[1, 1, 1, 1],
	[2, 1, 1, 1],
	[3, 1, 1, 1],
	[2, 0, 0, 0],
	[1, 1, 1, 0],
	[2, 2, 0, 1],
]
print(solution(n, build_frame))
"""
