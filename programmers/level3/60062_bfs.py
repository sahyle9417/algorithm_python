# 외벽 점검


from collections import deque


def solution(n, weak, dist):
	dist = sorted(dist, key=lambda x: -x)

	# bfs를 위한 큐
	q = deque([weak])

	# 남아있는 weak point들(remain)의 상태를 remain_set에 기록
	# 동일한 weak point들(remain)이 큐에 여러 번 들어가지 않도록 함
	remain_set = set()
	remain_set.add(tuple(weak))

	# 투입된 친구 수, 새로 추가된 친구의 이동 가능 거리
	for man_num, d in enumerate(dist, 1):

		q_size = len(q)
		for _ in range(q_size):

			# 수리하지 못한 weak point들
			remain = q.popleft()

			# 남아있는 weak point를 한 번씩 시작지점으로 설정함
			for start in remain:
				end = (start + d) % n

				# remain_tmp은 현재 선택된 친구가 수리하지 못하는 weak point들
				if start < end:
					remain_tmp = tuple(filter(lambda x: x < start or x > end, remain))
				else:  # end <= start
					remain_tmp = tuple(filter(lambda x: end < x < start, remain))

				# 모든 weak 수리 완료
				if len(remain_tmp) == 0:
					return man_num
				# 남아있는 weak point들(remain)의 상태가 remain_set에 이미 있다면 큐 삽입 X
				# 동일한 weak point들(remain)이 큐에 여러 번 들어가지 않도록 함
				# remain_set 안 써도 동일한 결과는 나오지만 시간 초과 발생
				elif remain_tmp not in remain_set:
					remain_set.add(remain_tmp)
					q.append(list(remain_tmp))
	return -1


"""
n = 12
weak = [1, 5, 6, 10]
dist = [1, 2, 3, 4]
print(solution(n, weak, dist))
"""
