#자물쇠와 열쇠


def rotate_key(key):
	size = len(key)
	rotated_key = [[0 for _ in range(size)] for _ in range(size)]

	for r in range(size):
		for c in range(size):
			rotated_key[r][c] = key[size-c-1][r]

	return rotated_key


def rotate_key_inplace(key):
	size = len(key)

	# rotate each layer
	for layer_i in range(size // 2):
		s = layer_i			 # start idx of layer
		e = size - 1 - layer_i  # end idx of layer

		for i in range(e - s):
			tmp = key[s][s + i]			 # 12시
			key[s][s + i] = key[e - i][s]   # 9시->12시
			key[e - i][s] = key[e][e - i]   # 6시->9시
			key[e][e - i] = key[s + i][e]   # 3시->6시
			key[s + i][e] = tmp			 # 12시->3시

	return key


# resize key to lock_size and move according to r_move and c_move
def move_key(key, lock_size, r_move, c_move):
	new_key = [[0 for _ in range(lock_size)] for _ in range(lock_size)]

	key_size = len(key)

	for r in range(key_size):
		for c in range(key_size):

			# skip out of bound element
			r_new = r + r_move
			if r_new < 0 or r_new >= lock_size:
				continue
			c_new = c + c_move
			if c_new < 0 or c_new >= lock_size:
				continue

			new_key[r_new][c_new] = key[r][c]

	return new_key


# check whether key and lock is matched or not
def is_matched(key, lock):
	for key_row, lock_row in zip(key, lock):
		for k, l in zip(key_row, lock_row):
			if k + l != 1:
				return False
	return True


def solution(key, lock):
	key_size = len(key)
	lock_size = len(lock)

	# rotate key
	for _ in range(4):
		key = rotate_key(key)

		# move key
		for r_move in range(-key_size + 1, lock_size):
			for c_move in range(-key_size + 1, lock_size):
				moved_key = move_key(key, lock_size, r_move, c_move)

				# check whether key and lock is matched or not
				if is_matched(moved_key, lock):
					return True

	return False


"""
def print_matrix(matrix):
	for row in matrix:
		print(" ".join(list(map(str, row))))
key = [
	[0, 0, 0],
	[1, 0, 0],
	[0, 1, 1]
]
lock = [
	[1, 1, 1],
	[1, 1, 0],
	[1, 0, 1]
]
print(solution(key, lock))
"""
