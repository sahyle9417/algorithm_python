from collections import deque


def solution(lines):
	logs = []

	# make log history with parsed time
	for line in lines:

		_, end, period = line.split(" ")
		end_h, end_m, end_s = end.split(":")

		# time unit : 0.001s
		parsed_end = int(end_h) * 3600000 + int(end_m) * 60000 + int(end_s.replace(".", ""))
		parsed_period = int(float(period[:-1]) * 1000)
		parsed_start = parsed_end - parsed_period + 1

		# log format : (time, is_transaction_starting_log)
		logs.append((parsed_start, True))   # transaction_starting_log
		logs.append((parsed_end, False))	# transaction_ending_log

	# sort log by time
	logs = sorted(logs, key=lambda x: x[0])
	logs = deque(logs)

	answer = 0
	window_start_count = 0

	# every log become candidate for window start
	while logs:
		window_start_log = logs.popleft()

		# found transaction starting log
		# transaction starting logs do not need to be window start
		if window_start_log[1]:
			window_start_count += 1
			continue

		# found transaction ending log
		# transaction ending log can be window start
		# count max number of logs in window (window size = 1000)
		count = window_start_count
		for log in logs:
			if log[0] > window_start_log[0] + 999:
				break
			if log[1]:
				count += 1

		answer = max(answer, count)

		# transaction ends
		window_start_count -= 1

	return answer


"""
lines = [
	"2016-09-15 20:59:57.421 0.351s",
	"2016-09-15 20:59:58.233 1.181s",
	"2016-09-15 20:59:58.299 0.8s",
	"2016-09-15 20:59:58.688 1.041s",
	"2016-09-15 20:59:59.591 1.412s",
	"2016-09-15 21:00:00.464 1.466s",
	"2016-09-15 21:00:00.741 1.581s",
	"2016-09-15 21:00:00.748 2.31s",
	"2016-09-15 21:00:00.966 0.381s",
	"2016-09-15 21:00:02.066 2.62s"
]
print(solution(lines))
"""
