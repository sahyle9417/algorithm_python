def possible(stones, k, mid):
	# stones : 각 stone에 적힌 숫자
	# k : 점프 가능 거리
	# mid : 건너갈 수 있는지 검사할 사람 수
	# gap : 연속으로 비어있는 stone 개수
	gap = 0

	for stone in stones:
		# 해당 stone이 사람들(mid) 수용 불가, 비어있는 stone 개수(gap) 증가
		if stone < mid:
			gap += 1
		# 해당 stone이 사람들(mid) 수용 가능, 비어있는 stone 개수(gap) 초기화
		else:
			gap = 0
		# 비어있는 stone 개수(gap)가 점프 가능 거리(k)와 같거나 커지면 실패
		if gap == k:
			return False

	return True


# 건너갈 수 있는 사람 수 이분탐색
def solution(stones, k):
	# 가능한 것으로 확인된 최댓값
	low = min(stones)
	# 불가능한 것으로 확인된 최솟값
	high = max(stones) + 1

	# low와 high가 1 차이나면 low가 정답
	while low + 1 < high:
		# low와 high가 2 이상 차이나면 mid는 항상 low보다 큼 (즉, 무한반복 안함)
		mid = (low + high) // 2
		if possible(stones, k, mid):
			low = mid
		else:
			high = mid

	return low


"""
stones = [2, 4, 5, 3, 2, 1, 4, 2, 5, 1]
k = 3
print(solution(stones, k))
"""
