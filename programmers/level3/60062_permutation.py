# 외벽 점검


from itertools import permutations


def solution(N, weak, dist):
	dist = sorted(dist, key=lambda x: -x)

	weak_num = len(weak)

	# 투입된 친구 수
	for man_num in range(1, len(dist) + 1):

		# 친구들 투입하는 순서의 모든 경우의 수 고려
		for man_perm in permutations(dist[:man_num], man_num):

			# 모든 weak point를 한 번씩 시작지점으로 설정
			for weak_idx in range(weak_num):
				man_idx = 0				 # 친구 인덱스
				man_start = weak[weak_idx]  # 현재 친구가 커버하는 weak point의 시작
				fixed = 0				   # 수리 완료된 weak point 개수

				# 모든 weak이 수리되거나 모든 man을 소진할 때까지 반복
				while fixed < weak_num and man_idx < man_num:
					current = weak[weak_idx]
					d = (current - man_start + N) % N

					# current가 현재 친구의 범위 안에 위치, 수리 완료
					if d <= man_perm[man_idx]:
						weak_idx = (weak_idx + 1) % weak_num
						fixed += 1
					# current가 현재 친구의 범위 밖에 위치, 다음 친구에게 넘김
					else:
						man_idx += 1
						man_start = current

				# 모든 weak 수리 완료
				if fixed == weak_num:
					return man_num

	return -1


"""
N = 12
weak = [1, 5, 6, 10]
dist = [1, 2, 3, 4]
print(solution(N, weak, dist))
"""
