from itertools import product


def match(user, regex):
	if len(user) != len(regex):
		return False

	for u, r in zip(user, regex):
		if r != "*" and u != r:
			return False

	return True


def solution(user_id, banned_id):
	answer = set()
	result = [[] for i in range(len(banned_id))]

	for i, regex in enumerate(banned_id):
		for u in user_id:
			if match(u, regex):
				result[i].append(u)

	result = list(product(*result))
	for res in result:
		res = set(res)
		if len(res) == len(banned_id):
			answer.add("".join(sorted(res)))

	return len(answer)


"""
user_id = ["frodo", "fradi", "crodo", "abc123", "frodoc"]
banned_id = ["fr*d*", "*rodo", "******", "******"]
print(solution(user_id, banned_id))
"""
