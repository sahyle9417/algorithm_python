from collections import deque


# 남북동서
dr = [1, -1, 0, 0]
dc = [0, 0, 1, -1]


def is_movable(state, d, board, size):
	for r, c in state:
		nr = r + dr[d]
		nc = c + dc[d]
		if 0 <= nr < size and 0 <= nc < size and board[nr][nc] == 0:
			continue
		else:
			return False
	return True


def move(state, d):
	moved = []
	for r, c in state:
		pos = (r + dr[d], c + dc[d])
		moved.append(pos)
	moved = sorted(moved)
	moved = tuple(moved)
	return moved


def rotate(state, d, mode):
	# 현재 상태에서 점 2개를 각각 한 번씩 고정된 회전축(pivot)으로 지정
	for r, c in state:
		# 현재 가로 -> 세로로 회전(남북)
		if mode == "-":
			new_pos = (r + dr[d], c)
		# 현재 세로 -> 가로로 회전(동서)
		else:  # elif mode == '|':
			new_pos = (r, c + dc[d])

		rotated = [(r, c), new_pos]
		rotated = sorted(rotated)
		rotated = tuple(rotated)
		yield rotated


def solution(board):
	# 초기 상태
	state = ((0, 0), (0, 1))

	# BFS를 위한 큐
	q = deque()
	q.append(state)

	# 동일한 state를 여러 번 다루지 않기 위해 기록
	visited = set()
	visited.add(state)

	size = len(board)
	time = 0

	while q:

		qlen = len(q)
		for _ in range(qlen):

			state = q.popleft()

			# 종료
			for pos in state:
				if pos == (size - 1, size - 1):
					return time

			# 네 방향으로 이동 또는 회전
			for d in range(4):

				# 주어진 방향(d)으로 이동 또는 회전 가능한지 확인
				# 이동 가능 조건과 회전 가능 조건이 동일
				movable = is_movable(state, d, board, size)

				# 주어진 방향(d)으로 이동 또는 회전 불가
				if not movable:
					continue

				# 주어진 방향(d)으로 이동한 결과를 큐에 추가
				moved = move(state, d)
				if moved not in visited:
					q.append(moved)
					visited.add(moved)

				# 현재 가로 상태이고 세로(남북)로 회전시킴
				rotated = []
				if state[0][0] == state[1][0]:
					if d == 0 or d == 1:
						rotated.extend(rotate(state, d, "-"))
				# 현재 세로 상태이고 가로(동서)로 회전시킴
				else:  # elif state[0][1] == state[1][1]:
					if d == 2 or d == 3:
						rotated.extend(rotate(state, d, "|"))

				# 주어진 방향(d)으로 회전한 결과들을 큐에 추가
				for r in rotated:
					if r not in visited:
						q.append(r)
						visited.add(r)

		time += 1


"""
board = [
	[0, 0, 0, 1, 1],
	[0, 0, 0, 1, 0],
	[0, 1, 0, 1, 1],
	[1, 1, 0, 0, 1],
	[0, 0, 0, 0, 0],
]
print(solution(board))
"""
