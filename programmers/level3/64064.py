from collections import deque


def solution(user_id, banned_id):
	user = tuple(user_id)
	regex = tuple(banned_id)
	state = tuple([user, regex])

	q = deque([state])
	visited = set()
	answer = 0

	while q:
		user, regex = q.popleft()

		if len(regex) == 0:
			answer += 1
			continue

		for u in user:
			for r in regex:

				if not match(u, r):
					continue

				new_user = remove(user, u)
				new_regex = remove(regex, r)
				new_state = tuple([new_user, new_regex])

				if new_state not in visited:
					visited.add(new_state)
					q.append(new_state)

	return answer


def match(user, regex):
	if len(user) != len(regex):
		return False

	for u, r in zip(user, regex):
		if r != "*" and u != r:
			return False

	return True


def remove(tuple_, element):
	list_ = list(tuple_)
	list_.remove(element)
	return tuple(list_)


"""
user_id = ["frodo", "fradi", "crodo", "abc123", "frodoc"]
banned_id = ["fr*d*", "*rodo", "******", "******"]
print(solution(user_id, banned_id))
"""
