from collections import defaultdict

def solution(clothes):
	category_to_cloth = defaultdict(list)
	for cloth, category in clothes:
		category_to_cloth[category].append(cloth)

	ret = 1
	for cloth_list in category_to_cloth.values():
		ret *= (len(cloth_list) + 1)
	# 아무것도 안입는 케이스 제거
	return ret - 1


#clothes = [["yellow_hat", "headgear"], ["blue_sunglasses", "eyewear"], ["green_turban", "headgear"]]
#clothes = [["crow_mask", "face"], ["blue_sunglasses", "face"], ["smoky_makeup", "face"]]
clothes = [["a1", "a"], ["a2", "a"], ["b1", "b"], ["b2", "b"], ["b3", "b"], ["c1", "c"], ]

print(solution(clothes))