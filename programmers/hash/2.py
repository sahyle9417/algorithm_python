
def solution(phone_book):
	# 길이순 정렬
	phone_book = sorted(phone_book, key=lambda x: len(x))
	phone_set = set()
	for phone_num in phone_book:
		for i in range(1, len(phone_num)):
			prefix = phone_num[:i]
			if prefix in phone_set:
				return False
		phone_set.add(phone_num)
	return True

#phone_book = ["97674223", "1195524421", "119"]
phone_book = ["123", "456", "789"]

print(solution(phone_book))