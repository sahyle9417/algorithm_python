from collections import Counter

# 동명이인 있을수 있으므로 set 대신 Counter 쓰자
def solution(participant, completion):
	participant = Counter(participant)
	completion = Counter(completion)
	incompletion = participant - completion
	return list(incompletion.keys())[0]

participant = ["leo", "kiki", "eden", "kiki"]
completion = ["kiki", "eden", "leo"]
print(solution(participant, completion))
