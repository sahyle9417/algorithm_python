from collections import defaultdict

def solution(genres, plays):
	length = len(genres)
	indices = [i for i in range(length)]

	genre_to_play = defaultdict(int)  # 각 장르의 재생 횟수
	genre_to_music = dict()  # 각 장르에 속한 노래의 재생 횟수, 인덱스

	for genre, play, idx in zip(genres, plays, indices):
		genre_to_play[genre] += play
		if genre not in genre_to_music:
			genre_to_music[genre] = [[], []]
		genre_to_music[genre][0].append(play)
		genre_to_music[genre][1].append(idx)

	# 재생횟수 기준으로 장르 정렬
	genre_to_play = sorted(genre_to_play.items(), key=lambda x: -x[1])

	ret = []
	for genre, _ in genre_to_play:
		# 각 장르의 노래 재생횟수 리스트와 노래 인덱스 리스트를 unpack (asterisk) 했다가
		# 다중 리스트 정렬을 위해 다시 zip 처리
		music = zip(*genre_to_music[genre])
		# 재생횟수 내림차순, 인덱스 오름차순 정렬
		music = sorted(music, key=lambda x: (-x[0], x[1]))
		# 각 장르당 1개는 무조건 출력
		ret.append(music[0][1])
		# 각 장르의 노래 개수가 2개 이상이면 2개 출력
		if len(music) > 1:
			ret.append(music[1][1])

	return ret


genres = ["classic", "pop", "classic", "classic", "pop"]
plays = [500, 600, 150, 800, 2500]
print(solution(genres, plays))