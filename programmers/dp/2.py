def solution(triangle):
	for r in range(1, len(triangle)):
		for c in range(0, r + 1):
			if c == 0:
				triangle[r][c] += triangle[r-1][c]
			elif c == r:
				triangle[r][c] += triangle[r-1][c-1]
			else:  # 0 < col < row
				triangle[r][c] += max(triangle[r-1][c-1], triangle[r-1][c])
	return max(triangle[-1])


triangle = [[7], [3, 8], [8, 1, 0], [2, 7, 4, 4], [4, 5, 2, 6, 5]]
print(solution(triangle))
