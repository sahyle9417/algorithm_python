
def solution(N, number):
	if N == number:
		return 1

	all_output = [0, {N}]

	for stage in range(2, 9):
		# x단계 결과는 N을 x번 반복한 숫자를 포함함
		curr_output = {int(str(N)*stage)}

		# x단계 결과는 아래의 연산 결과들을 모두 합친 것
		# 1단계 결과와 x-1단계 결과에 대한 사칙연산 결과,
		# 2단계 결과와 x-2단계 결과에 대한 사칙연산 결과, ...
		for half_stage in range(1, stage//2+1):
			for x in all_output[half_stage]:
				for y in all_output[stage-half_stage]:
					curr_output.add(x+y)
					curr_output.add(x-y)
					curr_output.add(y-x)
					curr_output.add(x*y)
					if x != 0:
						curr_output.add(y//x)
					if y != 0:
						curr_output.add(x//y)

		print(curr_output)
		if number in curr_output:
			return stage
		all_output.append(curr_output)
	return -1


print(solution(5, 12))
