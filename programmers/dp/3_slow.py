
# m : col num
# n : row num
def solution(m, n, puddles):
	dp = [[0 for _ in range(m)] for _ in range(n)]
	dp[0][0] = 1

	# 이 방법이 더 느림
	#for col, row in puddles:
	#	dp[row - 1][col - 1] = -1

	for stage in range(m + n):
		for row in range(stage + 1):
			col = stage - row
			if row >= n or col >= m:
				continue
			#if dp[row][col] == -1:  # 이게 더 느림
			if [col + 1, row + 1] in puddles:  # 특이하게도 이게 더 빠름
				dp[row][col] = 0
				continue
			if col > 0:
				dp[row][col] += dp[row][col - 1]
			if row > 0:
				dp[row][col] += dp[row - 1][col]

	#print(dp)
	return dp[n-1][m-1] % 1000000007


m = 4
n = 3
puddles = [[2, 2]]
print(solution(m, n, puddles))
