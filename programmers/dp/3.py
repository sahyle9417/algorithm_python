# m : col num
# n : row num
def solution(m, n, puddles):
	dp = [[0 for _ in range(m)] for _ in range(n)]
	dp[0][0] = 1
	for row in range(n):
		for col in range(m):
			if row == 0 and col == 0:
				continue
			if [col + 1, row + 1] in puddles:
				dp[row][col] = 0
				continue
			if row > 0:
				dp[row][col] += dp[row - 1][col]
			if col > 0:
				dp[row][col] += dp[row][col - 1]
	return dp[n - 1][m - 1] % 1000000007



m = 4
n = 3
puddles = [[2, 2]]
print(solution(m, n, puddles))
