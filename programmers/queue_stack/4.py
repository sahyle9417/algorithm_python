from collections import Counter

def solution(priorities, location):
	counter = Counter(priorities)
	queue = [(idx, priority) for idx, priority in enumerate(priorities)]
	printed = 0
	while True:
		idx, priority = queue.pop(0)
		if priority == max(counter.keys()):
			printed += 1
			counter[priority] -= 1
			# counter에서 value가 0이 되더라도 key는 사라지지 않음
			# 수동으로 제거해줘야 함
			if counter[priority] == 0:
				counter.pop(priority)
			if location == idx:
				return printed
		else:  # if p < max(counter.keys()):
			queue.append((idx, priority))
	return printed

"""
# 느리지만 이해하기 좀 더 쉬운 코드
def solution(priorities, location):
	# (idx, priority) 형태의 element를 가지는 큐
	queue =  [(i,p) for i,p in enumerate(priorities)]
	answer = 0
	while True:
		cur = queue.pop(0)
		if any(cur[1] < q[1] for q in queue):
			queue.append(cur)
		else:
			answer += 1
			if cur[0] == location:
				return answer
"""


priorities = [2, 1, 3, 2]
location = 2
priorities = [1, 1, 9, 1, 1, 1]
location = 0

print(solution(priorities, location))