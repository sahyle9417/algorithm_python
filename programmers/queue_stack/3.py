
def solution(bridge_length, weight, truck_weights):
	time = 0
	on_bridge = []
	on_time = []

	# 아직 다리에 못올라간 트럭이 있거나 다리에 트럭이 있으면 반복
	while truck_weights or on_bridge:
		time += 1

		# 맨 앞의 트럭이 다리 끝에 도달했다면 pop
		if on_bridge and on_time[0] + bridge_length == time:
			on_bridge.pop(0)
			on_time.pop(0)

		# 아직 트럭이 남았고 올라갈 수 있다면 append
		if truck_weights and sum(on_bridge) + truck_weights[0] <= weight:
			on_bridge.append(truck_weights.pop(0))
			on_time.append(time)

	return time



bridge_length = 100
weight = 100
truck_weights = [10]
truck_weights = [10,10,10,10,10,10,10,10,10,10]

bridge_length = 2
weight = 10
truck_weights = [7,4,5,6]

print(solution(bridge_length, weight, truck_weights))
