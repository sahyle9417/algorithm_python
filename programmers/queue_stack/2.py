import math

# 정답 코드
def solution(progresses, speeds):
	answer = []
	length = len(progresses)
	day_left_list = [math.ceil((100-a)/b) for a, b in zip(progresses, speeds)]
	front = 0
	for idx in range(length):
		if day_left_list[front] < day_left_list[idx]:
			answer.append(idx-front)
			front = idx
	answer.append(length-front)
	return answer


def solution2(progresses, speeds):
	day_left_list = list(map(lambda p, s: math.ceil((100 - p) / s), progresses, speeds))

	release_list = []

	prev_day_left = day_left_list[0]
	release = 1
	for curr_day_left in day_left_list[1:]:
		if prev_day_left < curr_day_left:
			release_list.append(release)
			prev_day_left = curr_day_left
			release = 1
		else:
			release += 1
	release_list.append(release)
	return release_list


progresses = [93, 30, 55]
speeds = [1, 30, 5]

progresses = [95, 90, 99, 99, 80, 99]
speeds = [1, 1, 1, 1, 1, 1]

progresses = [95, 90, 99, 99, 80, 99]
speeds = [1, 1, 1, 1, 1, 1]

print(solution2(progresses, speeds))
