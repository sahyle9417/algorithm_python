def solution(n, times):
	low = 1
	high = max(times) * n

	while low < high:
		# 소요 예측 시간
		mid = (low + high) // 2

		# 주어진 시간(mid)에 처리할 수 있는 사람 수
		#people = sum([mid // t for t in times])
		people = 0
		for t in times:
			people += mid // t
			if people >= n:
				break

		# 시간 딱 맞거나 남음
		# people == n이 성립하더라도 mid가 최적이 아닐 수 있음
		# 재배치를 통해 더 빨라질 수 있기 때문
		if people >= n:
			high = mid

		# 시간이 더 필요
		else:  # if people < n:
			low = mid + 1

	return low






# 시간초과
def slow_solution(n, times):
	current_time = 1
	while True:
		for time in times:
			if current_time % time == 0:
				n -= 1
				if n == 0:
					return current_time
		current_time += 1


n = 6
times = [7, 10]
print(solution(n, times))