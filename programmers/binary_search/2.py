import math


def solution(distance, rocks, n):
	answer = 0

	# 간격 하한은 0부터 distance 사이
	low = 0
	high = distance

	rocks = sorted(rocks)
	rocks.append(distance)

	while low <= high:
		remove = 0
		prev = 0
		min_gap = math.inf

		# 매 반복 시 적용할 간격(gap)의 하한
		# 간격(gap)이 mid보다 작다면 뒤쪽의 바위 제거
		mid = (low + high) // 2

		for curr in rocks:

			gap = curr - prev

			# 간격이 간격 하한(mid)보다 작다면 i번째 바위 제거
			if gap < mid:
				remove += 1

			# 간격이 간격 하한(mid)와 같거나 크면 바위 유지
			# 최소 간격(min_gap) 갱신 (min_gap은 항상 mid보다 크거나 같음)
			# prev를 현재 바위로 갱신
			else:
				min_gap = min(min_gap, gap)
				prev = curr

		# 목표 개수보다 많이 제거한 경우
		# 간격 하한의 범위를 현재값(mid)보다 낮게 설정(low ~ mid-1)
		# 제거되는 돌의 개수를 줄여야함
		if remove > n:
			high = mid - 1

		# 목표 개수보다 적게 제거한 경우
		# 간격 하한의 범위를 현재값(mid)보다 높게 설정(mid+1 ~ high)
		# 제거되는 돌의 개수를 늘려야함
		# 목표 개수와 동일하게 제거한 경우에도 여기서 처리
		# 간격 하한(mid)의 최대값을 구하는 것이 목표이므로
		# 목표 개수와 동일하게 제거했더라도 멈추지 않고 더 높은 간격 하한 시도
		else:
			low = mid + 1
			answer = min_gap

	return answer
