def solution(n, costs):
	ans = 0
	costs.sort(key=lambda x: x[2])
	connected = {costs[0][0]}
	while len(connected) < n:
		print(costs)
		print(connected)
		print()
		for i, cost in enumerate(costs):
			if cost[0] in connected and cost[1] in connected:
				continue
			# 이 조건이 왜 필요하지?
			if cost[0] in connected or cost[1] in connected:
				connected.update([cost[0], cost[1]])  # add elements to set
				ans += cost[2]
				costs.pop(i)
				break
	return ans


n = 4
costs = [
	[0, 1, 1],
	[0, 2, 2],
	[1, 2, 5],
	[1, 3, 1],
	[2, 3, 8]
]
print(solution(n, costs))
