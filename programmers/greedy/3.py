
def solution(name):
	target = [min(ord(c) - ord('A'), ord('Z') - ord(c) + 1) for c in name]
	length = len(target)

	move = target[0]
	target[0] = 0
	cursor = 0

	while sum(target) > 0:

		left_idx = cursor
		left_move = 0
		while target[left_idx] == 0:
			left_idx = left_idx - 1 if left_idx != 0 else length - 1
			left_move += 1

		right_idx = cursor
		right_move = 0
		while target[right_idx] == 0:
			right_idx = right_idx + 1 if right_idx != length - 1 else 0
			right_move += 1

		cursor = left_idx if left_move < right_move else right_idx
		move += left_move if left_move < right_move else right_move
		move += target[cursor]
		target[cursor] = 0

	return move

name = 'JAN'
print(solution(name))