
def array_solution(n, lost, reserve):
	# 1번 인덱스부터 사용
	arr = [0] * (n + 1)
	for idx in lost:
		arr[idx] -= 1
	for idx in reserve:
		arr[idx] += 1
	ret = 0
	for idx in range(1, n + 1):
		if arr[idx] == 0 or arr[idx] == 1:
			ret += 1
		else:  # elif arr[idx] == -1:
			if idx > 1 and arr[idx-1] == 1:
				#arr[idx-1] -= 1
				ret += 1
			elif idx < n and arr[idx+1] == 1:
				arr[idx+1] -= 1
				ret += 1
	return ret


def set_solution(n, lost, reserve):
	# 변수명 덮어쓰기 자제하기
	# 여기서 lost_set 대신 lost 이름 썼다가
	# set로 바뀐 lost에 대해 다시 set를 씌우는 실수를 범했음
	lost_set = set(lost) - set(reserve)
	reserve_set = set(reserve) - set(lost)
	for reserve_idx in reserve_set:
		if reserve_idx - 1 in lost_set:
			lost_set.remove(reserve_idx - 1)
		elif reserve_idx + 1 in lost_set:
			lost_set.remove(reserve_idx + 1)
	return n - len(lost_set)


def set_solution2(n, lost, reserve):
	# 변수명 덮어쓰기 자제하기
	# 여기서 lost_set 대신 lost 이름 썼다가
	# set로 바뀐 lost에 대해 다시 set를 씌우는 실수를 범했음
	lost_set = set(lost) - set(reserve)
	reserve_set = set(reserve) - set(lost)
	borrow = 0
	for lost_idx in lost_set:
		if lost_idx - 1 in reserve_set:
			reserve_set.remove(lost_idx - 1)
			borrow += 1
		elif lost_idx + 1 in reserve_set:
			reserve_set.remove(lost_idx + 1)
			borrow += 1
	return n - len(lost_set) + borrow


n = 5
lost = [2, 4]
reserve = [3]

n = 3
lost = [3]
reserve = [1]

n = 5
lost = [2, 4]
reserve = [1, 3, 5]
print(solution(n, lost, reserve))