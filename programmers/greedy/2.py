def solution(number, k):
	ret = []

	for (i, num) in enumerate(number):
		while ret and ret[-1] < num and k > 0:
			ret.pop()
			k -= 1
		# 속도 향상을 위한 부분
		#if k == 0:
		#	ret += number[i:]
		#	break
		ret.append(num)

	ret = ret[:-k] if k > 0 else ret
	ret = ''.join(ret)
	return ret


# 너무 느려서 시간초과
from itertools import combinations
def slow_solution(number, k):
	digits = [digit for digit in number]
	print(digits)
	combs = combinations(digits, len(digits) - k)
	ret = set()
	for comb in combs:
		ret.add(int(''.join(comb)))
	print(ret)
	return str(max(ret))


number = '4177252841'
k = 4
print(solution(number, k))