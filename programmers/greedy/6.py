
def solution(routes):
	answer = 0
	# 차량이 나간(진출) 시점 기준으로 오름차순 정렬
	routes.sort(key=lambda x: x[1])

	# -30000부터 카메라 놓을 수 있으므로 카메라 위치 초기값은 -30001
	last_camera = -30001

	for start, end in routes:
		# last_camera가 이 차량을 커버할 수 없음
		# 해당 차량의 진출 시점에 새로운 카메라 추가
		if last_camera < start:
			answer += 1
			last_camera = end
	return answer
