
def solution(people, limit):
	# 2명까지밖에 못태우므로 무거운사람1 (+ 가벼운사람1) 이렇게 태움
	people = sorted(people, reverse=True)
	heavy_idx = 0
	light_idx = len(people) - 1
	ret = 0
	while heavy_idx <= light_idx:
		heavy = people[heavy_idx]
		light = people[light_idx]
		if heavy + light <= limit:
			light_idx -= 1
		heavy_idx += 1
		ret += 1
	return ret

people = [70, 50, 80, 50]
limit = 100
people = [70, 50, 80, 50, 10, 20, 30]
limit = 100

people = [100]
limit = 100
print(solution(people, limit))