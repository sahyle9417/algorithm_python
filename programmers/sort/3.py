def solution(citations):
	citations = sorted(citations)
	total_paper_num = len(citations)
	for paper_idx in range(total_paper_num):
		citation = citations[paper_idx]  # 해당 논문의 인용 횟수, 단조 증가
		paper_num = total_paper_num - paper_idx  # citation 회 이상 인용된 논문 수, 단조 감소
		if citation >= paper_num:
			return paper_num
	return 0


from collections import Counter

def slow_solution(citations):
	total_citation = sum(citations)
	citation_to_freq = Counter(citations)

	for h_index in range(max(citations), -1, -1):
		h_or_above = dict(filter(lambda x: x[0] >= h_index, citation_to_freq.items()))
		h_or_above = sum(h_or_above.values())
		h_below = total_citation - h_or_above
		if h_or_above >= h_index:
			return h_index
	return 0

citations = [3, 0, 6, 1, 5]
print(solution(citations))