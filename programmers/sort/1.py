
def solution(array, commands):
	answer = []
	for i, j, k in commands:
		sub_array = array[i-1:j]
		print(sub_array)
		sub_array = sorted(sub_array)
		answer.append(sub_array[k-1])

	return answer



array = [1, 5, 2, 6, 3, 7, 4]
commands = [[2, 5, 3], [4, 4, 1], [1, 7, 3]]
print(solution(array, commands))