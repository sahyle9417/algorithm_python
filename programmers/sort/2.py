
def solution(numbers):
	# 문자열로 변환
	numbers = list(map(str, numbers))
	# 3, 30의 경우 정답은 330이지만 x*3을 해주지 않으면 303이 나온다
	# element의 범위는 최대 4자리므로 반복을 통해 강제로 4자리를 채워준다
	# 3333, 30303030에 대해 문자열 비교하면 정상적으로 330 순서가 된다
	numbers = sorted(numbers, key=lambda x: x*4, reverse=True)
	# 0,0,0,0과 같은 input에 대해 int로 변환했다가 다시 str으로 돌아오면 깔끔하게 바뀌어 있다
	return str(int(''.join(numbers)))

numbers = [6, 10, 2]
numbers = [3, 30, 34, 5, 9]
print(solution(numbers))