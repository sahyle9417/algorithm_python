# 실패율


from collections import Counter, defaultdict


def solution(N, stages):
	trial = len(stages)
	failed_stages = Counter(stages)

	result = []

	for stage in range(1, N + 1):

		if trial == 0:
			rate = 0
		else:
			fail = failed_stages[stage]
			rate = fail / trial
			trial -= fail

		result.append([stage, rate])

	result = sorted(result, key=lambda x: -x[1])
	result = [stage for stage, _ in result]

	return result


"""
N = 5
stages = [2, 1, 2, 6, 2, 4, 3, 3]
N = 4
stages = [4,4,4,4,4]
print(solution(N, stages))
"""
