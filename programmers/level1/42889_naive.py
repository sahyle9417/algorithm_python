# 실패율


from collections import Counter, defaultdict


def solution(N, stages):
	failed_stages = Counter(stages)
	tried_stages = defaultdict(int)

	for failed_stage, freq in failed_stages.items():
		for tried_stage in range(1, failed_stage + 1):
			tried_stages[tried_stage] += freq

	result = []
	for stage in range(1, N + 1):
		trial = tried_stages[stage]
		fail = failed_stages[stage]
		if trial == 0:
			rate = 0
		else:
			rate = fail / trial
		result.append([stage, rate])

	result = sorted(result, key=lambda x: -x[1])
	result = [stage for stage, _ in result]

	return result


"""
N = 5
stages = [2, 1, 2, 6, 2, 4, 3, 3]
N = 4
stages = [4,4,4,4,4]
print(solution(N, stages))
"""
