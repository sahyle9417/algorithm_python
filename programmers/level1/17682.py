import re


def solution(dartResult):
	power_parser = {"S": 1, "D": 2, "T": 3}
	option_parser = {"": 1, "*": 2, "#": -1}

	# p = re.compile("(\d+)([SDT])([*#]?)")
	p = re.compile("([0-9]+)([SDT])([*#]?)")
	tokens = p.findall(dartResult)

	for i, token in enumerate(tokens):
		digit, power, option = token
		if option == "*" and i > 0:
			tokens[i - 1] *= 2
		tokens[i] = (int(digit) ** power_parser[power]) * option_parser[option]

	return sum(tokens)


"""
print(solution("1S2D*3T"))
print(solution("1D2S#10S"))
print(solution("1D2S0T"))
print(solution("1S*2T*3S"))
print(solution("1D#2S*3S"))
print(solution("1T2D3D#"))
print(solution("1D2S3T*"))
"""
