def solution(n, arr1, arr2):
	answer = []
	for dec1, dec2 in zip(arr1, arr2):
		# 10진수 int에 대해 bit 연산자 사용 가능
		dec = dec1 | dec2
		binary = bin(dec)[2:]
		binary = binary.rjust(n, "0")
		binary = binary.replace("0", " ")
		binary = binary.replace("1", "#")
		answer.append(binary)
	return answer


"""
n = 5
arr1 = [9, 20, 28, 18, 11]
arr2 = [30, 1, 21, 17, 28]
print(solution(n, arr1, arr2))
"""
