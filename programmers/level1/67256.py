# 키패드 누르기


def solution(numbers, hand):
	answer = []

	left_num = {1, 4, 7}
	right_num = {3, 6, 9}

	# save hand position
	pos = dict()

	# change special character to number (to get distance easily)
	pos["L"] = 10
	pos["R"] = 12

	for num in numbers:
		next_hand = None

		# fixed result
		if num in left_num:
			next_hand = "L"
		elif num in right_num:
			next_hand = "R"

		# changeable result
		else:
			# change 0 to 11 (to get distance easily)
			num = 11 if num == 0 else num

			# get distance from each hand and compare
			left_dist = get_dist(pos["L"], num)
			right_dist = get_dist(pos["R"], num)

			if left_dist < right_dist:
				next_hand = "L"
			elif left_dist > right_dist:
				next_hand = "R"

			# both hands have same distance
			else:
				next_hand = "L" if hand == "left" else "R"

		answer.append(next_hand)
		pos[next_hand] = num

	return "".join(answer)


def get_dist(pos1, pos2):
	row1, col1 = divmod(pos1 - 1, 3)
	row2, col2 = divmod(pos2 - 1, 3)
	return abs(row1 - row2) + abs(col1 - col2)


"""
numbers = [1, 3, 4, 5, 8, 2, 1, 4, 5, 9, 5]
hand = "right"
numbers = [7, 0, 8, 2, 8, 3, 1, 5, 7, 6, 2]
hand = "left"
print(solution(numbers, hand))
"""
