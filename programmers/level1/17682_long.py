from collections import deque


def solution(dartResult):
	dartResult = deque(dartResult)

	power_parser = {"S": 1, "D": 2, "T": 3}
	option_parser = {"*": 2, "#": -1}

	token = []
	tokens = []
	end_of_token = False

	while dartResult:
		c = dartResult.popleft()

		# digit
		if c.isdigit():
			# 1st digit
			if len(token) == 0:
				token.append(int(c))
			# 2nd digit
			else:  # if len(token) > 0:
				token[0] = token[0] * 10 + int(c)

		# power (S, D, T)
		elif c in power_parser:
			token.append(power_parser[c])
			# end of token
			if len(dartResult) == 0 or dartResult[0].isdigit():
				end_of_token = True

		# option (*, #)
		else:  # if c in option_parser:
			token.append(option_parser[c])
			end_of_token = True

		# end_of_token
		if end_of_token:
			tokens.append(token)
			token = []
			end_of_token = False

	for idx, token in enumerate(tokens):
		digit = token[0]
		power = token[1]
		option = 1 if len(token) == 2 else token[2]

		tokens[idx] = (digit ** power) * option

		if option == 2 and idx > 0:
			tokens[idx - 1] *= 2

	return sum(tokens)


"""
print(solution("1S2D*3T"))
print(solution("1D2S#10S"))
print(solution("1D2S0T"))
print(solution("1S*2T*3S"))
print(solution("1D#2S*3S"))
print(solution("1T2D3D#"))
print(solution("1D2S3T*"))
"""
