# 크레인 인형뽑기 게임


from collections import deque


def solution(board, moves):
	answer = 0
	size = len(board)
	polled = deque()

	for col in moves:
		col -= 1
		for row in range(size):
			cell = board[row][col]
			if cell != 0:
				board[row][col] = 0
				if polled and polled[-1] == cell:
					polled.pop()
					answer += 2
				else:
					polled.append(cell)
				break
	return answer


"""
board = [
	[0,0,0,0,0],
	[0,0,1,0,3],
	[0,2,5,0,1],
	[4,2,4,4,2],
	[3,5,1,3,1]
]
moves = [1,5,3,5,1,2,1,4]
print(solution(board, moves))
"""
