from collections import deque


def solution(n, computers):
	visit = [False for _ in range(n)]
	answer = 0
	for start_node in range(n):
		if visit[start_node]:
			continue
		answer += 1
		q = deque([start_node])
		visit[start_node] = True
		while q:
			q_size = len(q)
			for i in range(q_size):
				node = q.popleft()
				for neighbor, edge in enumerate(computers[node]):
					if edge == 1 and node != neighbor and not visit[neighbor]:
						q.append(neighbor)
						visit[neighbor] = True
	return answer
