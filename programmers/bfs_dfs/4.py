from collections import defaultdict


def solution(tickets):

	routes = defaultdict(list)
	for depart, arrive in tickets:
		routes[depart].append(arrive)

	# 도착지 기준으로 내림차순 정렬 (차후에 pop으로 오름차순으로 뽑을 것임)
	for depart in routes:
		routes[depart].sort(reverse=True)  # 아래와 동일
		#routes[depart] = sorted(routes[depart], reverse=True)

	stack = ['ICN']
	ret = []

	while stack:
		top = stack[-1]

		# top에서 출발할 수 있는 티켓이 있으면
		# 알파벳 순 가장 앞서는 도착지를 stack에 추가
		if top in routes and len(routes[top]) > 0:
			# 도착지 리스트에서 pop하면 오름차순으로 추출됨
			stack.append(routes[top].pop())

		# top에서 출발할 수 있는 티켓이 없으면 stack에서 빼서 ret에 추가
		else:  # if top not in routes or len(routes[top]) == 0:
			ret.append(stack.pop())

	ret.reverse()
	return ret
