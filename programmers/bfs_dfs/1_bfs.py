def solution(numbers, target):
	# 최초의 prev_result는 0
	queue = [0]
	for number in numbers:
		# 현재 idx에서 임시로 사용할 queue
		tmp_queue = []
		while queue:
			prev_result = queue.pop()
			tmp_queue.append(prev_result + number)
			tmp_queue.append(prev_result + number*(-1))
		queue = tmp_queue
	return queue.count(target)


numbers = [1, 1, 1, 1, 1]
target = 3
print(solution(numbers, target))
