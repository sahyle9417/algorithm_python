def solution(n, computers):
	visit = [False for _ in range(n)]

	answer = 0
	for start_node in range(n):
		if visit[start_node]:
			continue
		dfs(start_node, visit, computers)
		answer += 1
	return answer


def dfs(node, visit, computers):
	visit[node] = True
	for neighbor, edge in enumerate(computers[node]):
		if edge == 1 and neighbor != node and not visit[neighbor]:
			dfs(neighbor, visit, computers)
