from collections import defaultdict


def dfs(graph, N, depart, footprint):
	# 재귀 탈출 조건
	if len(footprint) == N + 1:
		return footprint

	for idx, arrive in enumerate(graph[depart]):
		# 도착지 리스트에서 임시로 제거
		graph[depart].pop(idx)

		# footprint 복제하고 다음 목적지 추가한 뒤 재귀 호출
		tmp = footprint.copy()
		tmp.append(arrive)
		ret = dfs(graph, N, arrive, tmp)

		# 제거했던 도착지를 다시 리스트 같은 위치에 그대로 추가
		graph[depart].insert(idx, arrive)

		# 모든 티켓을 사용(len(footprint) == N + 1)한 경우 연쇄적으로 재귀 탈출
		if ret is not None:
			return ret


def solution(tickets):
	answer = []

	graph = defaultdict(list)

	N = len(tickets)
	for depart, arrive in tickets:
		graph[depart].append(arrive)

	# 도착지 기준으로 오름차순 정렬
	for depart, _ in tickets:
		graph[depart].sort()

	answer = dfs(graph, N, "ICN", ["ICN"])
	return answer
