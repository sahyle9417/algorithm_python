def dfs(idx, numbers, target, value):
	# 재귀 끝내기
	if idx == len(numbers):
		return 1 if target == value else 0
	answer = 0
	answer += dfs(idx + 1, numbers, target, value + numbers[idx])
	answer += dfs(idx + 1, numbers, target, value - numbers[idx])
	return answer


def solution(numbers, target):
	return dfs(0, numbers, target, 0)


numbers = [1, 1, 1, 1, 1]
target = 3
print(solution(numbers, target))
