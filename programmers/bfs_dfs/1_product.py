from itertools import product


# dfs보다 빠름
def solution(numbers, target):
	# [1,2,3] -> [(1, -1), (2, -2), (3, -3)]
	pairs = [(x, -x) for x in numbers]

	# iterable에 *(asterisk)를 붙이면 unpack 수행 (맨 바깥쪽 차원이 사라짐)
	# product : 각 iterable에서 하나씩 고르는 모든 경우의 수 생성
	# 생성된 각 경우의 수에 대해 합해서 모든 경우의 수의 결과 산출
	possible_results = list(map(sum, product(*pairs)))

	# 산출된 모든 경우의 수에서 target의 개수 count
	return possible_results.count(target)


numbers = [1, 1, 1, 1, 1]
target = 3
print(solution(numbers, target))
