from collections import deque


def solution(begin, target, words):
	# 이거 빼먹어서 테케 1개만 실패해서 당황했었음
	# 코너 케이스 잘 챙기자
	if target not in words:
		return 0
	char_sets = [set() for _ in range(len(begin))]
	for word in words:
		for char_idx, char in enumerate(word):
			char_sets[char_idx].add(char)
	q = deque([list(begin)])
	answer = 0
	while q:
		q_size = len(q)
		for _ in range(q_size):
			s = q.popleft()
			if ''.join(s) == target:
				return answer

			for char_idx, char_set in enumerate(char_sets):
				for c in char_set:
					if s[char_idx] != c:
						tmp = s.copy()
						tmp[char_idx] = c
						if ''.join(tmp) in words:
							q.append(tmp)
		answer += 1
		if answer == 11:
			answer = 0
			break

	return answer
