
def solution(brown, yellow):
	total = brown + yellow
	for brown_h in range(3, total // 3 + 1):
		brown_w = total // brown_h
		if (brown_h - 2) * (brown_w - 2) == yellow and brown_h * brown_w == brown + yellow:
			return [brown_w, brown_h]


brown = 24
yellow = 24
print(solution(brown, yellow))
