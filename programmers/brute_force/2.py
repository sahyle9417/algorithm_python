from itertools import permutations
from math import sqrt

def solution(numbers):
	digits = [digit for digit in numbers]
	ret = set()
	for l in range(1, len(digits) + 1):
		pairs = permutations(digits, l)
		for pair in pairs:
			n = int(''.join(pair))
			if isPrime(n):
				ret.add(n)
	print(ret)
	return len(ret)

# 처음에 2가 소수가 아닌 것으로 반환하는 오류를 범했음
# 헷갈릴 것 같으면 짝수 스킵 스킬 쓰지말자
# 속도 차이도 10% 정도밖에 안됨
def isPrime(number):
	if number < 2:
		return False
	if number == 2:
		return True
	if number % 2 == 0:
		return False
	divisor = 3
	while divisor * divisor <= number:
		if number % divisor == 0:
			return False
		divisor += 2
	return True

"""def isPrime(n):
	k = sqrt(n)
	if n < 2:
		return False
	for i in range(2, int(k)+1):
		if n % i == 0:
			return False
	return True"""

numbers = "977"
print(solution(numbers))