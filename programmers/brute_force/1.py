
def solution(answers):
	patterns = [[1, 2, 3, 4, 5], [2, 1, 2, 3, 2, 4, 2, 5], [3, 3, 1, 1, 2, 2, 4, 4, 5, 5]]
	corrects = [0, 0, 0]
	for answer_idx, answer in enumerate(answers):
		for person_idx in range(3):
			pattern = patterns[person_idx]
			if answer == pattern[answer_idx % len(pattern)]:
				corrects[person_idx] += 1
	ret = []
	for person_idx, correct in enumerate(corrects, start=1):
		if correct == max(corrects):
			ret.append(person_idx)
	return ret

answers = [1,2,3,4,5]
#answers = [1,3,2,4,2]
print(solution(answers))

