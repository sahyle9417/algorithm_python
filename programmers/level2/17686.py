# 파일명 정렬
import re


def solution(files):
	#files = list(map(split_filename, files))
	# * : 선행 패턴의 0회 이상 반복
	# + : 선행 패턴의 1회 이상 반복
	# [0-9], \d : 숫자
	# [0-9]+, \d+ : 숫자 반복
	files = [re.split('(\d+)', file) for file in files]
	#files = [re.split('([0-9]+)', file) for file in files]
	files = sorted(files, key=lambda x: (x[0].lower(), int(x[1])))
	files = [''.join(file) for file in files]
	return files


def split_filename(filename):
	start_idx = -1
	end_idx = -1
	for idx in range(len(filename)):
		c = filename[idx]
		if c.isdigit():
			start_idx = idx
			break
	for idx in range(start_idx + 1, len(filename)):
		c = filename[idx]
		if not c.isdigit():
			end_idx = idx
			break

	# 이거 놓쳐서 한참 삽질했음, 코너 케이스는 항상 조심하자
	# slicing의 시작인덱스가 가용 인덱스보다 커도 에러 안뜨고 빈리스트를 반환
	if end_idx == -1:
		end_idx = len(filename)

	head = filename[:start_idx]
	num = filename[start_idx:end_idx]
	tail = filename[end_idx:]

	return [head, num, tail]


"""
files = ['img12.png', 'img10.png', 'img02.png', 'img1.png', 'IMG01.GIF', 'img2.JPG']
files = ['img12', 'img10', 'img02', 'img1', 'IMG01', 'img2']
files = ['F-5 Freedom Fighter', 'B-50 Superfortress', 'A-10 Thunderbolt II', 'F-14 Tomcat']
print(solution(files))
"""
