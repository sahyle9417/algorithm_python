# 후보키
from itertools import combinations
from collections import deque


def solution(relation):
	col_num = len(relation[0])

	col_comb = deque()
	for r in range(1, col_num+1):
		col_comb.extend(deque(map(set, combinations(range(col_num), r))))

	answer = 0

	while col_comb:
		cols = col_comb.popleft()
		is_valid = True
		unique_row = set()

		for row in relation:
			selected_row = tuple(row[col] for col in cols)
			# 중복 행 발견, 유일성 만족하지 않음
			if selected_row in unique_row:
				is_valid = False
				break
			unique_row.add(selected_row)

		# 후보키 발견
		if is_valid:
			answer += 1

			# 최소성 만족하지 않는 cols들 제거
			col_comb = deque(c for c in col_comb if c & cols != cols)

	return answer


relation = [
	['100', 'ryan', 'music', '2'],
	['200', 'apeach', 'math', '2'],
	['300', 'tube', 'computer', '3'],
	['400', 'con', 'computer', '4'],
	['500', 'muzi', 'music', '3'],
	['600', 'apeach', 'music', '2']
]
print(solution(relation))
