# 뉴스 클러스터링
from collections import Counter


def solution(str1, str2):
	counter1 = Counter(tokenize(str1))
	counter2 = Counter(tokenize(str2))

	union_token = set(counter1.keys()) | set(counter2.keys())
	union_counter = dict()
	for token in union_token:
		union_counter[token] = max(counter1[token], counter2[token])

	if sum(union_counter.values()) == 0:
		return 65536

	intersect_token = set(counter1.keys()) & set(counter2.keys())
	intersect_counter = dict()
	for token in intersect_token:
		intersect_counter[token] = min(counter1[token], counter2[token])

	print(union_counter)
	print(intersect_counter)
	jaccard = sum(intersect_counter.values()) / sum(union_counter.values())
	return int(jaccard * 65536)


def tokenize(str):
	return [str[i:i+2].upper() for i in range(len(str)-1) if str[i:i+2].isalpha()]


"""
def cleanse_token(str):
	ret = []
	for c in str:
		if 'A' <= c <= 'Z':
			ret.append(c)
		elif 'a' <= c <= 'z':
			ret.append(c.upper())
		else:
			return None
	return ''.join(ret)

def tokenize(str):
	ret = []
	for i in range(len(str) - 1):
		token = cleanse_token(str[i:i + 2])
		if token is not None:
			ret.append(token)
	return ret
"""


str1 = 'FRANCE'
str2 = 'french'

#str1 = 'handshake'
#str2 = 'shake hands'

#str1 = 'aa1+aa2'
#str2 = 'AAAA12'

#str1 = 'E=M*C^2'
#str2 = 'e=m*c^2'

print(solution(str1, str2))
