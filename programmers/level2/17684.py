# 압축


def solution(msg):
	parser = {chr(ord('A') + i): i + 1 for i in range(26)}
	"""parser = dict()
	for i in range(26):
		parser[chr(ord('A') + i)] = i + 1"""
	max_idx = 26

	idx = 0
	length = len(msg)
	answer = []

	while idx < length:

		sub_len = 2
		while idx + sub_len <= length and msg[idx:idx+sub_len] in parser:
			sub_len += 1

		if idx + sub_len <= length:
			max_idx += 1
			parser[msg[idx: idx + sub_len]] = max_idx

		parsed = parser[msg[idx: idx + sub_len - 1]]
		answer.append(parsed)
		idx = idx + sub_len - 1

	return answer


msg = 'KAKAO'
msg = 'TOBEORNOTTOBEORTOBEORNOT'
print(solution(msg))
