# 오픈채팅방


def solution(record):
	answer = []
	uid_to_name = dict()

	for record_line in record:
		record_line = record_line.split(' ')
		if record_line[0] != 'Leave':
			uid_to_name[record_line[1]] = record_line[2]

	for record_line in record:
		record_line = record_line.split(' ')
		op = record_line[0]
		uid = record_line[1]
		if op == 'Enter':
			answer.append(f'{uid_to_name[uid]}님이 들어왔습니다.')
		elif op == 'Leave':
			answer.append(f'{uid_to_name[uid]}님이 나갔습니다.')
	return answer


record = ["Enter uid1234 Muzi", "Enter uid4567 Prodo","Leave uid1234","Enter uid1234 Prodo","Change uid4567 Ryan"]
print(solution(record))
