from collections import Counter
from itertools import combinations


def solution(orders, course):
	answer = []

	# 하나의 코스에 포함시킬 메뉴 개수
	for length in course:

		# 각 메뉴가 몇명의 손님에 의해 주문되었는지 기록
		menu = Counter()
		for order in orders:
			# 메뉴 오름차순 정렬
			order = sorted(order)
			menu += Counter(combinations(order, length))

		if len(menu) == 0:
			continue

		# 주어진 길이(length)의 메뉴 조합들의 최대 빈도가 2미만(1)이면 스킵
		max_freq = max(menu.values())
		if max_freq == 1:
			continue

		# 최대 빈도의 메뉴 조합들 선택
		menu = ["".join(k) for k, v in menu.items() if v == max_freq]
		answer.extend(menu)

	return sorted(answer)


"""
orders = ["ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH"]
course = [2, 3, 4]
orders = ["ABCDE", "AB", "CD", "ADE", "XYZ", "XYZ", "ACD"]
course = [2,3,5]
print(solution(orders, course))
"""
