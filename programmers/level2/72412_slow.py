from collections import defaultdict


def solution(info, query):
	# 각 item에 해당하는 지원자의 인덱스들을 반환
	item_to_idx = defaultdict(set)

	# 각 지원자의 점수 기록
	idx_to_score = dict()

	for idx, person in enumerate(info):
		person = person.split(" ")

		score = int(person.pop(-1))
		idx_to_score[idx] = score

		for item in person:
			item_to_idx[item].add(idx)
	
	answer = []
	# 초기엔 모든 지원자의 인덱스를 포함
	init_idx = set(range(len(info)))

	for q in query:
		q = q.split(" ")
		chosen_idx = init_idx.copy()

		# 점수로 필터링
		score = int(q.pop(-1))
		tmp = filter(lambda x: x[1] >= score, idx_to_score.items())
		chosen_idx &= set(map(lambda x: x[0], tmp))

		# 각 item에 대해 필터링
		for item in q:
			if item == "-" or item == "and":
				continue
			chosen_idx &= item_to_idx[item]

		# 모든 조건에 부합하는 지원자의 수 반환
		answer.append(len(chosen_idx))

	return answer


"""
info = [
	"java backend junior pizza 150",
	"python frontend senior chicken 210",
	"python frontend senior chicken 150",
	"cpp backend senior pizza 260",
	"java backend junior chicken 80",
	"python backend senior chicken 50",
]
query = [
	"java and backend and junior and pizza 100",
	"python and frontend and senior and chicken 200",
	"cpp and - and senior and pizza 250",
	"- and backend and senior and - 150",
	"- and - and - and chicken 100",
	"- and - and - and - 150",
]
print(solution(info, query))
"""
