# 방금그곡


def remove_sharp(melody):
	parser = {
		'C#':'c',
		'D#':'d',
		'F#':'f',
		'G#':'g',
		'A#':'a',
	}
	for before, after in parser.items():
		melody = melody.replace(before, after)
	return melody


def preprocess(musicinfos):
	ret = []

	for music_idx, musicinfo in enumerate(musicinfos):
		start, end, title, melody = musicinfo.split(',')

		# get duration
		start_h, start_m = map(int, start.split(':'))
		end_h, end_m = map(int, end.split(':'))
		duration = (end_h - start_h) * 60 + (end_m - start_m)

		# repeat melody based on duration
		melody = remove_sharp(melody)
		repeat, remain = divmod(duration, len(melody))
		melody = melody * repeat + melody[:remain]

		ret.append([duration, music_idx, title, melody])

	return ret


def solution(m, musicinfos):
	# duration, music_idx, title, melody
	musicinfos = preprocess(musicinfos)

	# sort by duration(desc), music_idx(asc)
	musicinfos = sorted(musicinfos, key=lambda x: (-x[0], x[1]))

	m = remove_sharp(m)
	for _, _, title, melody in musicinfos:
		if m in melody:
			return title
	return "(None)"

"""
m = 'ABCDEFG'
musicinfos = ['12:00,12:14,HELLO,CDEFGAB', '13:00,13:05,WORLD,ABCDEF']
m = 'CC#BCC#BCC#BCC#B'
musicinfos = ['03:00,03:30,FOO,CC#B', '04:00,04:08,BAR,CC#BCC#BCC#B']
m = 'ABC'
musicinfos = ['12:00,12:14,HELLO,C#DEFGAB', '13:00,13:05,WORLD,ABCDEF']
print(solution(m, musicinfos))
"""
