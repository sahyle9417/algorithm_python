from itertools import combinations as combi
from collections import defaultdict


def solution(infos, queries):

	# 점수 이외의 정보(info)를 key로 점수(score) 리스트를 value로 사용하는 dict
	info_to_score = defaultdict(list)

	for info in infos:
		# 점수(score)와 점수 이외의 정보(info)로 분리
		info = info.split()
		score = int(info[-1])
		info = info[:-1]

		# info_num : 점수 이외의 정보(info) 4개 중 key에 포함시킬 정보의 개수
		# info_comb 개수 : 16 (4C0 + 4C1 + 4C2 + 4C3 + 4C4)
		for info_num in range(5):
			for info_comb in combi(info, info_num):
				# 선택한 info 조합을 문자열로 변환하여 key로 사용
				info_key = "".join(info_comb)
				info_to_score[info_key].append(score)

	# 동일 info에 대해 점수들을 오름차순 정렬 (이진탐색)
	for info_key in info_to_score:
		info_to_score[info_key].sort()

	answer = []
	for query in queries:
		# 점수(query_score)와 점수 이외의 정보(query)로 분리
		query = query.split(" ")
		query_score = int(query[-1])
		query = query[:-1]

		# query에서 "and", "-" 제거 후 문자열로 변환
		query = [q for q in query if q != "and" and q != "-"]
		query = "".join(query)

		# query와 일치하는 사람 없음
		if query not in info_to_score:
			answer.append(0)
			continue

		# 현재 query와 일치하는 score들의 리스트
		scores = info_to_score[query]

		# 이분탐색으로 query_score 이상의 점수 개수 구하기
		# low : 아직 확인되지 않은 인덱스들 중 최솟값
		# high : query_score보다 큰 것으로 확인된 인덱스
		low = 0
		high = len(scores)

		# low와 high가 같아질 때까지 반복
		while low < high:
			mid = (low + high) // 2
			if query_score <= scores[mid]:
				high = mid
			else:
				low = mid + 1

		# low와 high는 query_score 이상인 인덱스 중 최솟값
		answer.append(len(scores) - low)

	return answer


"""
info = [
	"java backend junior pizza 150",
	"python frontend senior chicken 210",
	"python frontend senior chicken 150",
	"cpp backend senior pizza 260",
	"java backend junior chicken 80",
	"python backend senior chicken 50",
]
query = [
	"java and backend and junior and pizza 100",
	"python and frontend and senior and chicken 200",
	"cpp and - and senior and pizza 250",
	"- and backend and senior and - 150",
	"- and - and - and chicken 100",
	"- and - and - and - 150",
]
print(solution(info, query))
"""
