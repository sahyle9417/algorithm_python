# 124 나라의 숫자


def solution(n):
	ret = []
	while n:  # while n > 0:
		div, mod = divmod(n-1, 3)
		ret.append('124'[mod])
		n = int(div)
	ret.reverse()
	return ''.join(ret)