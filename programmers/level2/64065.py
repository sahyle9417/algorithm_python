# 튜플
from collections import Counter


def solution(s):
	s = s.replace('{', '')
	s = s.replace('}', '')
	counter = Counter(s.split(','))
	counter = sorted(counter.items(), key=lambda x: -x[1])
	answer = [int(element) for element, freq in counter]
	return answer


s = '{{4,2,3},{3},{2,3,4,1},{2,3}}'
print(solution(s))
