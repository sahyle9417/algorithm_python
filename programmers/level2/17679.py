# 프렌즈4블록


def solution(m, n, board):
	board = [list(row) for row in board]
	answer = 0
	while True:
		#for row in board:
		#	print(' '.join(row))
		remove = set()
		for row in range(m-1):
			for col in range(n-1):
				block1 = board[row][col]
				block2 = board[row][col+1]
				block3 = board[row+1][col]
				block4 = board[row+1][col+1]
				if block1 == '_' or block2 == '_' or block3 == '_' or block4 == '_':
					continue
				if block1 == block2 == block3 == block4:
					# cannot add list to set (list is mutable)
					# can add tuple to set (tuple is immutable)
					remove.add((row, col))
					remove.add((row, col+1))
					remove.add((row+1, col))
					remove.add((row+1, col+1))
		if len(remove) == 0:
			break
		#print(remove)
		answer += len(remove)

		# 블록 제거
		for row, col in remove:
			board[row][col] = '_'

		# 중력에 맞게 떨어뜨리기
		for col in range(n):
			write_row = m-1
			for read_row in range(m-1, -1, -1):
				if board[read_row][col] == '_':
					continue
				else:
					board[write_row][col] = board[read_row][col]
					write_row -= 1
			for row in range(write_row, -1, -1):
				board[row][col] = '_'

	return answer


m = 4
n = 5
board = ['CCBDE',
		 'AAADE',
		 'AAABF',
		 'CCBBF']

m = 6
n = 6
board = ['TTTANT',
		 'RRFACC',
		 'RRRFCC',
		 'TRRRAA',
		 'TTMMMF',
		 'TMMTTJ']

print(solution(m, n, board))
