# 수식 최대화
from itertools import permutations


def tokenize_expression(expression):
	# result example : [100, '-', 200, '*', 300, '-', 500, '+', 20]
	splited_expression = []
	token = []
	for c in expression:
		if c.isdigit():
			token.append(c)
		else:
			splited_expression.append(int(''.join(token)))
			splited_expression.append(c)
			token = []
	splited_expression.append(int(''.join(token)))
	return splited_expression


# 문자열 형태의 수식을 eval()함수에 넣으면 계산해줌
def calculate(first, operator, second):
	if operator == '+':
		return first + second
	elif operator == '-':
		return first - second
	else:   # elif operator == '*':
		return first * second


def solution(expression):
	ret = -1
	expression = tokenize_expression(expression)
	for priority in permutations('+-*', 3):
		tmp = expression.copy()
		for operator in priority:
			# 동일한 연산자 찾아서 앞뒤 피연산자와 계산한 결과로 대체
			idx = 1
			while idx < len(tmp):
				# 연산자 불일치 시 2칸씩 점프
				if tmp[idx] != operator:
					idx += 2
					continue
				# 연산자 일치 -> 앞뒤 피연산자와 계산한 결과로 대체
				# 문자열 형태의 수식을 eval()함수에 넣으면 계산해줌
				tmp[idx - 1] = calculate(*tmp[idx - 1: idx + 2])
				# 인덱스 번호로 pop할 땐 항상 뒤에서부터
				tmp.pop(idx + 1)
				tmp.pop(idx)
		ret = max(ret, abs(tmp[0]))
	return ret


# print(solution('100-200*300-500+20'))
