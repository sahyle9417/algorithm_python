# 괄호 변환


def solution(p):
	if p == '':
		return p
	# 생략 가능
	#if is_valid(p):
	#	return p
	ret = []
	opened = 1 if p[0] == '(' else -1
	for idx in range(1, len(p)):
		opened += 1 if p[idx] == '(' else -1
		if opened == 0:
			u = p[:idx+1]
			v = p[idx+1:]
			if is_valid(u):
				ret.append(u)
				ret.append(solution(v))
			else:  # u is invalid
				ret.append('(')
				ret.append(solution(v))
				ret.append(')')
				ret.append(flip(u[1:-1]))
			break
	return ''.join(ret)


def is_valid(p):
	opened = 0
	for c in p:
		opened += 1 if c == '(' else -1
		if opened < 0:
			return False
	if opened == 0:
		return True
	# 문제 조건에 따라 이런 경우 발생하지 않음
	else:
		return False


def flip(p):
	ret = []
	for c in p:
		if c == '(':
			ret.append(')')
		else:
			ret.append('(')
	return ''.join(ret)


p = '(()())()'
p = ')('
p = '()))((()'
print(solution(p))
