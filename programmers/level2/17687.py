# n진수 게임


def solution(n, t, m, p):
	all_list = []
	decimal = 0
	while len(all_list) < t * m:
		all_list.extend(parse(decimal, n))
		decimal += 1
	#print(all_list)

	answer = []
	for i in range(p-1, (p-1)+m*t, m):
		answer.append(all_list[i])

	return ''.join(answer)


def parse(decimal, base):
	# 이거 빼먹어서 삽질했음, 주의하자
	if decimal == 0:
		return ['0']

	ret = []
	while decimal:
		decimal, remain = divmod(decimal, base)
		if remain <= 9:
			ret.append(str(remain))
		else:
			ret.append(chr(ord('A') + (remain - 10)))
	ret.reverse()
	return ret


"""
n, t, m, p = 2, 4, 2, 1
n, t, m, p = 16, 16, 2, 1
n, t, m, p = 16, 16, 2, 2
print(solution(n, t, m, p))
"""
