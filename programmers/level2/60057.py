# 문자열 압축
import math


def solution(s):
	length = len(s)

	if length == 1:
		return 1

	answer = math.inf
	for split_len in range(1, length // 2 + 1):
		split_list = []
		split_repeat = []
		for split_idx in range(length // split_len):
			if split_idx == 0:
				split_list.append(s[:split_len])
				split_repeat.append(1)
				continue
			split_str = s[split_idx * split_len: (split_idx+1) * split_len]
			if split_str == split_list[-1]:
				split_repeat[-1] += 1
			else:
				split_list.append(split_str)
				split_repeat.append(1)

		if length % split_len > 0:
			split_list.append(s[-1 * (length % split_len):])
			split_repeat.append(1)

		local_answer = 0
		for str, repeat in zip(split_list, split_repeat):
			local_answer += len(str)
			if repeat > 1:
				local_answer += int(math.log10(repeat)) + 1
		answer = min(answer, local_answer)

	return answer


s = 'ababcdcdababcdcd'
s = 'aabbaccc'
s = 'abcabcdede'
s = 'a'
print(solution(s))
