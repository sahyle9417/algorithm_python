from collections import defaultdict


def solution(n, results):
	win = defaultdict(set)
	lose = defaultdict(set)

	for winner, loser in results:
		win[winner].add(loser)
		lose[loser].add(winner)

	for winner, loser_set in win.items():
		prev_loser_set = loser_set.copy()
		while True:
			new_loser_set = set()
			for loser in prev_loser_set:
				# 나한테 지는 노드가 이기는 노드는 나도 이김
				if loser in win:
					new_loser_set |= win[loser]
			prev_len = len(win[winner])
			win[winner] |= new_loser_set
			if prev_len == len(win[winner]):
				break
			prev_loser_set = new_loser_set

	for loser, winner_set in lose.items():
		prev_winner_set = winner_set.copy()
		while True:
			new_winner_set = set()
			for winner in prev_winner_set:
				# 나한테 이기는 노드가 지는 노드는 나도 짐
				if winner in lose:
					new_winner_set |= lose[winner]
			prev_len = len(lose[loser])
			lose[loser] |= new_winner_set
			if prev_len == len(lose[loser]):
				break
			prev_winner_set = new_winner_set

	ret = 0
	for idx in range(1, n + 1):
		if len(win[idx]) + len(lose[idx]) == n - 1:
			ret += 1

	return ret


n = 5
results = [[4, 3], [4, 2], [3, 2], [1, 2], [2, 5]]
print(solution(n, results))
