from collections import defaultdict, deque


def solution(n, edge):
	graph = defaultdict(list)
	for node1, node2 in edge:
		graph[node1].append(node2)
		graph[node2].append(node1)

	visit = [False for _ in range(n + 1)]
	q = deque([1])
	while True:
		q_size = len(q)
		for _ in range(q_size):
			# popleft 대신 pop 써서 한참 삽질함
			# 스택이 아닌 큐라는 것에 유의
			node = q.popleft()
			visit[node] = True
			for neighbor in graph[node]:
				if not visit[neighbor]:
					q.append(neighbor)
					visit[neighbor] = True
		if len(q) == 0:
			return q_size


n = 6
edge = [[3, 6], [4, 3], [3, 2], [1, 3], [1, 2], [2, 4], [5, 2]]
print(solution(n, edge))
