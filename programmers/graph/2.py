from collections import defaultdict


def solution(n, results):

	win = defaultdict(set)
	lose = defaultdict(set)

	for winner, loser in results:
		win[winner].add(loser)
		lose[loser].add(winner)

	for i in range(1, n + 1):
		# i를 이긴 노드(winner)들은 i가 이긴 노드들(win[i])도 이긴다
		for winner in lose[i]:
			win[winner] |= (win[i])
		# i에게 진 노드(loser)들은 i가 진 노드들(lose[i])에게도 진다
		for loser in win[i]:
			lose[loser] |= (lose[i])

	answer = 0
	for i in range(1, n + 1):
		if len(win[i]) + len(lose[i]) == n - 1:
			answer += 1

	return answer


n = 5
results = [[4, 3], [4, 2], [3, 2], [1, 2], [2, 5]]
print(solution(n, results))
