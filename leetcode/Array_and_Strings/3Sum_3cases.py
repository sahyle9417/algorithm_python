from bisect import bisect_left
from collections import Counter
from typing import List


class Solution:
	def threeSum(self, nums: List[int]) -> List[List[int]]:
		answer = []
		count = Counter(nums)
		nums = sorted(count)

		for idx, num in enumerate(nums):

			# case 1. three zeros
			if num == 0 and count[num] >= 3:
				answer.append((0, 0, 0))

			# case 2. two numbers are the same
			if num != 0 and count[num] >= 2 and -2 * num in count:
				answer.append((num, num, 0 - 2 * num))

			# case 3. not any of the three numbers are the same
			remain = -1 * num
			min_val = remain - nums[-1]
			max_val = remain / 2

			min_idx = bisect_left(nums, min_val, idx + 1)
			max_idx = bisect_left(nums, max_val, min_idx)

			for num2 in nums[min_idx:max_idx]:
				num3 = remain - num2
				if num3 in count:
					answer.append((num, num2, num3))

		return answer


"""
solution = Solution()
print(solution.threeSum([-1, 0, 1, 2, -1, -4]))
print(solution.threeSum([0, 0, 0, 0]))
"""
