from typing import List
import math


class Solution:
	def increasingTriplet(self, nums: List[int]) -> bool:
		num1 = math.inf
		num2 = math.inf

		for num in nums:
			if num > num2:
				return True
			elif num > num1:
				num2 = num
			else:  # if num < num1:
				num1 = num

		return False


"""
solution = Solution()
print(solution.increasingTriplet([1, 2, 3, 4, 5]))
print(solution.increasingTriplet([5, 4, 3, 2, 1]))
print(solution.increasingTriplet([2, 1, 5, 0, 4, 6]))
"""
