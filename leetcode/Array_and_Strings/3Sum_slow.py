from typing import List


class Solution:

	# 하위 30% 가량의 느린 속도
	def threeSum(self, nums: List[int]) -> List[List[int]]:
		answer = set()

		nums = sorted(nums)
		length = len(nums)

		for idx1, num1 in enumerate(nums[:-2]):

			# speed up 10%
			if num1 > 0:
				break

			idx2 = idx1 + 1
			idx3 = length - 1

			while idx2 < idx3:
				num2 = nums[idx2]
				num3 = nums[idx3]

				total = num1 + num2 + num3
				if total < 0:
					idx2 += 1
				elif total > 0:
					idx3 -= 1
				else:  # total == 0
					answer.add((num1, num2, num3))
					idx2 += 1
					idx3 -= 1

		return answer


"""
solution = Solution()
print(solution.threeSum([-1, 0, 1, 2, -1, -4]))
print(solution.threeSum([0, 0, 0, 0]))
"""
