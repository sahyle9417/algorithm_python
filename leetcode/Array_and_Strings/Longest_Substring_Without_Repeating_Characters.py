from typing import Dict


class Solution:
	def lengthOfLongestSubstring(self, s: str) -> int:
		answer = 0

		# 각 char가 마지막으로 등장한 위치(idx) 기록
		char_to_idx = dict()
		# substr의 시작 인덱스
		start_idx = 0

		for idx, c in enumerate(s):
			# 현재 substr 범위 내에 c가 이미 있다면 해당 위치 직후부터 substr 시작
			# start_idx 이전의 기록은 현재 substr에 영향주면 안됨 (이거 놓쳐서 한참 삽질)
			if c in char_to_idx and start_idx <= char_to_idx[c]:
				start_idx = char_to_idx[c] + 1
			else:
				answer = max(answer, idx - start_idx + 1)
			char_to_idx[c] = idx

		return answer


"""
solution = Solution()
print(solution.lengthOfLongestSubstring("abcabcbb"))
print(solution.lengthOfLongestSubstring("bbbbb"))
print(solution.lengthOfLongestSubstring("pwwkew"))
print(solution.lengthOfLongestSubstring("tmmzuxt"))
print(solution.lengthOfLongestSubstring(""))
"""
