from typing import List


class Solution:
	def setZeroes(self, matrix: List[List[int]]) -> None:
		row_num = len(matrix)
		col_num = len(matrix[0])

		zero_row = set()
		zero_col = set()

		for row_idx in range(row_num):
			for col_idx in range(col_num):
				if matrix[row_idx][col_idx] == 0:
					zero_row.add(row_idx)
					zero_col.add(col_idx)

		for row_idx in range(row_num):
			if row_idx in zero_row:
				matrix[row_idx] = [0] * col_num
				continue
			for col_idx in range(col_num):
				if col_idx in zero_col:
					matrix[row_idx][col_idx] = 0

"""
solution = Solution()
solution.setZeroes([[1, 1, 1], [1, 0, 1], [1, 1, 1]])
solution.setZeroes([[0, 1, 2, 0], [3, 4, 5, 2], [1, 3, 1, 5]])
"""
