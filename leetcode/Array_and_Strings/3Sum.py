from collections import Counter
from typing import List


class Solution:
	def threeSum(self, nums: List[int]) -> List[List[int]]:
		result = []

		counter = Counter(nums)
		if 0 in counter and counter[0] >= 3:
			result.append([0, 0, 0])

		# pos = [num for num in counter if num > 0]
		# neg = [num for num in counter if num < 0]

		pos = []
		neg = []
		for num in counter:
			if num > 0:
				pos.append(num)
			elif num < 0:
				neg.append(num)

		for p in pos:
			for n in neg:
				other = -(p + n)
				if other in counter:
					if other == p and counter[p] >= 2:
						result.append([n, p, p])
					elif other == n and counter[n] >= 2:
						result.append([n, n, p])
					elif n < other < p:
						result.append([n, other, p])
		return result


"""
solution = Solution()
print(solution.threeSum([-1, 0, 1, 2, -1, -4]))
print(solution.threeSum([0, 0, 0, 0]))
"""
