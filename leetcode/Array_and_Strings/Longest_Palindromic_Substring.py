class Solution:

	def longestPalindrome(self, s: str) -> str:

		def _get_palindrome(left, right):
			while left >= 0 and right < s_length:
				if s[left] == s[right]:
					left -= 1
					right += 1
				else:
					break
			return s[left + 1 : right]

		answer = ""
		s_length = len(s)

		for pivot in range(s_length):

			# 홀수 회문의 left는 pivot - 1
			# 짝수 회문의 left는 pivot
			for left in [pivot - 1, pivot]:
				palindrome = _get_palindrome(left, pivot + 1)
				if len(palindrome) > len(answer):
					answer = palindrome

		return answer


"""
solution = Solution()
print(solution.longestPalindrome("babad"))
print(solution.longestPalindrome("cbbd"))
print(solution.longestPalindrome("a"))
print(solution.longestPalindrome("ac"))
"""
