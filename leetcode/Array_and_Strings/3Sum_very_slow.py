from typing import Dict, List
from collections import Counter


class Solution:

	# 하위 10% 가량의 느린 속도
	def threeSum(self, nums: List[int]) -> List[List[int]]:
		nums = Counter(nums)
		answer = set()

		while nums:
			num1 = self.pop(nums)
			tmp = nums.copy()
			while tmp:
				num2 = self.pop(tmp)
				num3 = 0 - num1 - num2
				if num3 in tmp:
					answer.add(tuple([num1, num2, num3]))

		return answer

	def pop(self, counter: Dict[int, int]):
		key, value = counter.popitem()
		if value > 1:
			counter[key] = value - 1
		return key


"""
solution = Solution()
print(solution.threeSum([-1, 0, 1, 2, -1, -4]))
print(solution.threeSum([0, 0, 0, 0]))
"""
