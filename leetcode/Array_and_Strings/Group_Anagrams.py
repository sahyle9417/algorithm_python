from collections import defaultdict
from typing import List


class Solution:
	def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
		grouper = defaultdict(list)
		for org in strs:
			master = str(sorted(org))
			grouper[master].append(org)
		return grouper.values()


"""
solution = Solution()
print(solution.groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]))
"""
