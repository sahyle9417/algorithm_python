from collections import Counter
from typing import List


class Solution:
	def leastInterval(self, tasks: List[str], n: int) -> int:
		# freq : 각 task들의 빈도
		freq = list(Counter(tasks).values())
		# max_freq : 빈도 최댓값
		max_freq = max(freq)
		# max_freq_element_num : 최빈 task의 개수
		max_freq_element_num = freq.count(max_freq)

		# (n + 1) * (max_freq - 1) : 최빈 task들을 제외한 나머지 task 수행 시간
		# max_freq_element_num : 최빈 task만 남은 상태에서 최빈 task 수행 시간
		ans = (n + 1) * (max_freq - 1) + max_freq_element_num

		# ans 계산식은 n+1의 시간동안 모든 tasks를 한번씩 순회할 수 있다는 가정이 깔려있음
		# 하지만 task의 종류가 n + 1보다 많다면 위의 가정이 깨져서 정답보다 작은 값이 나올 수 있음
		# (n+1의 시간동안 다 순회하지 못한 tasks들을 마치 다 처리한 것처럼 취급했으니)
		# 예시) tasks=["A", "A", "B", "B", "C"], n=1 -> ans:4
		# 즉 결과는 아래 2가지 케이스 중 더 큰 값을 반환하면 됨
		# 1. ans 연산의 Idle Time 개수가 n+1 시간동안 순회 못한 task 개수보다 많다면 ans가 정답
		# 2. ans 연산의 Idle Time 개수가 n+1 시간동안 순회 못한 task 개수보다 적다면 Idle Time 없는 len(tasks)가 정답
		return max(ans, len(tasks))


"""
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B"], n=2))
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B"], n=0))
print(Solution().leastInterval(tasks=["A", "A", "A", "A", "A", "A", "B", "C", "D", "E", "F", "G"], n=2))
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B", "C", "C", "C", "D", "D", "E"], n=2))
print(Solution().leastInterval(tasks=["A", "A", "B", "B", "C"], n=1))
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B", "C", "D"], n=2))
"""
