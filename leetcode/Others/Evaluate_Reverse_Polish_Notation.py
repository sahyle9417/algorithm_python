from collections import deque
from typing import List


class Solution:
	def evalRPN(self, tokens: List[str]) -> int:
		nums = deque()
		ops = {"+", "-", "*", "/"}
		for token in tokens:
			if token in ops:
				num2 = nums.pop()
				num1 = nums.pop()
				res = self.calc(num1, token, num2)
				nums.append(res)
			else:
				nums.append(int(token))
			print(nums)
		return nums.pop()

	def calc(self, num1: int, op: str, num2: int) -> int:
		if op == "+":
			return num1 + num2
		elif op == "-":
			return num1 - num2
		elif op == "*":
			return num1 * num2
		else:
			return int(num1 / num2)


"""
tokens = ["2", "1", "+", "3", "*"]
print(Solution().evalRPN(tokens))
tokens = ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
print(Solution().evalRPN(tokens))
tokens = ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
print(Solution().evalRPN(tokens))
tokens = ["0", "3", "/"]
print(Solution().evalRPN(tokens))
"""
