class Solution:
	def getSum(self, a: int, b: int) -> int:
		carry = 0
		result = 0

		# 숫자는 하위 31비트로 표현하며 32번째 비트(MSB)는 부호부
		for i in range(32):

			# a와 b에서 i번째 비트 추출
			ai = (a >> i) & 1
			bi = (b >> i) & 1

			# si는 결과(result)의 i번째 비트
			# carry, ai, bi 중 1의 개수가 홀수면 si는 1
			si = carry ^ ai ^ bi

			# carry, ai, bi 중 1의 개수가 2개 이상이면 carry는 1
			carry = (ai & bi) | (ai & carry) | (bi & carry)

			# si를 결과(result)의 i번째 비트에 기록
			result |= (si << i)

		# 32번째 비트(si)가 1이면 실제 연산 결과는 음수
		# 하지만 파이썬은 정수 길이 제한이 없어서 32번째 비트가 1이어도 이를 부호가 아닌 숫자로 인식
		# 그러므로 33번째 이후의 모든 비트들도 1로 채워줘야 함
		if si == 1:

			# 2의 보수에서 전체 비트를 1로 채우면 -1
			# -1에 0xFFFFFFFF(하위 32비트만 1로 채운 비트열)을 XOR하면
			# 33번째 이후의 모든 비트를 1로 채운 비트열이 반환
			# 해당 비트열을 result에 OR 해주면 result의 33번째 이후의 모든 비트를 1로 채움
			result |= (-1 ^ 0xFFFFFFFF)

		return result
