from collections import Counter
from typing import List


class Solution:
	def leastInterval(self, tasks: List[str], n: int) -> int:
		time = 0
		task_ctr = Counter(tasks)

		while task_ctr:

			# 빈도 기준 내림차순 정렬
			task_ctr = dict(sorted(task_ctr.items(), key=lambda x: -x[1]))

			# 실제 task 수행 시간과 idle time
			task_time = min(len(task_ctr), n + 1)
			idle_time = n + 1 - task_time

			time += task_time

			# 빈도 기준 상위 n+1개의 task 수행 (task 개수가 n+1보다 작아도 문제 없음)
			# 순회 대상에 pop 수행 시 런타임 에러 발생하지만
			# pop 수행할 대상(task_ctr)을 직접 순회하지 않고 별도로 추출된 key 리스트를 순회하면 문제 없음
			for task in list(task_ctr)[: n + 1]:
				if task_ctr[task] == 1:
					task_ctr.pop(task)
				else:
					task_ctr[task] -= 1

			time += idle_time

		# 맨 마지막에는 idle 불필요
		time -= idle_time

		return time


"""
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B"], n=2))
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B"], n=0))
print(Solution().leastInterval(tasks=["A", "A", "A", "A", "A", "A", "B", "C", "D", "E", "F", "G"], n=2))
print(Solution().leastInterval(tasks=["A", "A", "A", "B", "B", "B", "C", "C", "C", "D", "D", "E"], n=2))
print(Solution().leastInterval(tasks=["A", "A", "B", "B", "C"], n=1))
"""
