from collections import defaultdict
from typing import List


class Solution:
	def majorityElement(self, nums: List[int]) -> int:
		#return sorted(Counter(nums).items(), key=lambda x: -x[1])[0][0]
		threshold = len(nums) / 2

		answer = 0
		max_freq = 0
		ctr = defaultdict(int)

		for num in nums:
			ctr[num] += 1
			# 정답 발견
			if ctr[num] >= threshold:
				return num
			# 최빈 요소 갱신
			if ctr[num] > max_freq:
				answer = num
				max_freq = ctr[num]

		return answer


#print(Solution().majorityElement([3, 2, 3]))
#print(Solution().majorityElement([2, 2, 1, 1, 1, 2, 2]))
