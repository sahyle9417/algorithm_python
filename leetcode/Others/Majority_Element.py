from collections import defaultdict
from typing import List


class Solution:
	def majorityElement(self, nums: List[int]) -> int:
		# Majority 원소는 전체의 절반을 초과하여 점유하므로
		# 정렬 시 정중앙엔 반드시 해당 원소가 존재한다.
		nums = sorted(nums)
		return nums[len(nums)//2]


#print(Solution().majorityElement([3, 2, 3]))
#print(Solution().majorityElement([2, 2, 1, 1, 1, 2, 2]))
