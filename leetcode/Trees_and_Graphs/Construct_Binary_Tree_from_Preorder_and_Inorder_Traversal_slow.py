from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:
	def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
		root_val = preorder[0]
		root_idx = inorder.index(root_val)
		root = TreeNode(root_val)

		inorder_left = inorder[:root_idx]
		inorder_right = inorder[root_idx + 1 :]

		left_len = root_idx
		right_len = len(inorder_right)

		preorder_left = preorder[1 : left_len + 1]
		preorder_right = preorder[left_len + 1 :]

		if left_len > 0:
			left = self.buildTree(preorder_left, inorder_left)
		else:
			left = None

		if right_len > 0:
			right = self.buildTree(preorder_right, inorder_right)
		else:
			right = None

		root.left = left
		root.right = right

		return root


"""
preorder = [3, 9, 20, 15, 7]
inorder = [9, 3, 15, 20, 7]
Solution().buildTree(preorder, inorder)
"""
