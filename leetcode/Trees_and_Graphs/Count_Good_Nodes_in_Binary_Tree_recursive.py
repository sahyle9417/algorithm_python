from typing import List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        def dfs(node, curr_max):
            if not node:
                return 0
            curr_cnt = int(curr_max <= node.val)
            curr_max = max(curr_max, node.val)
            left_cnt = dfs(node.left, curr_max)
            right_cnt = dfs(node.right, curr_max)
            return curr_cnt + left_cnt + right_cnt
        return dfs(root, root.val)


"""
def list_to_tree(node_list: List) -> TreeNode:
    for idx, val in enumerate(node_list):
        if val is None or val == "null":
            continue
        node_list[idx] = TreeNode(val)
        if idx == 0:
            continue
        parent, lr_idx = divmod(idx - 1, 2)
        if lr_idx == 0:
            node_list[parent].left = node_list[idx]
        else:  # lr_idx == 1:
            node_list[parent].right = node_list[idx]
    return node_list[0]


root = list_to_tree([10,9,8,7,"null",6,5])
print(Solution().goodNodes(root))
"""
