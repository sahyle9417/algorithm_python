from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:
	def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
		def recursive_build(pre_start, in_start, length):
			if length == 0:
				return None

			root_val = preorder[pre_start]
			root = TreeNode(root_val)

			# dict 안쓰고 아래와 같이 찾기도 가능 (dict가 더 빠름)
			# root_idx = inorder.index(root_val, in_start, in_start + length)
			root_idx = val_to_idx[root_val]

			left_len = root_idx - in_start
			right_len = length - left_len - 1

			root.left = recursive_build(
				pre_start=pre_start + 1,
				in_start=in_start,
				length=left_len,
			)

			root.right = recursive_build(
				pre_start=pre_start + left_len + 1,
				in_start=in_start + left_len + 1,
				length=right_len,
			)

			return root

		# 값(val)을 inorder idx로 변환
		# 이 부분 때문에 unique일 때만 가능
		val_to_idx = dict()
		for idx, val in enumerate(inorder):
			val_to_idx[val] = idx

		return recursive_build(pre_start=0, in_start=0, length=len(preorder))


"""
Solution().buildTree(preorder=[3, 9, 20, 15, 7], inorder=[9, 3, 15, 20, 7])
Solution().buildTree(preorder=[1, 2, 3], inorder=[2, 3, 1])
"""
