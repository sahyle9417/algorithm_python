from collections import deque
from typing import List, Deque


# Definition for a Node.
class Node:
	def __init__(
		self,
		val: int = 0,
		left: "Node" = None,
		right: "Node" = None,
		next: "Node" = None,
	):
		self.val = val
		self.left = left
		self.right = right
		self.next = next


class Solution:
	def connect(self, root: "Node") -> "Node":
		if not root:
			return

		# 주어진 level에서 가장 왼쪽 끝에 있는 노드
		level_head = root

		# 다음 level이 존재하는 동안 반복
		while level_head.left:
			node = level_head

			# 현재 level을 순회
			while node:

				# Case 1
				# 좌측 자식의 next는 우측 자식
				node.left.next = node.right

				# Case 2
				# 우측 자식의 next는 내 next의 좌측 자식
				# 내가 next를 가져야 우측 자식의 next 존재
				if node.next:
					node.right.next = node.next.left

				# 현재 level 내에서 다음(next) 노드로 이동
				node = node.next

			# 다음 level로 이동
			level_head = level_head.left

		return root


"""
def list_to_tree(list_: List[int]) -> List[Node]:
	for idx, val in enumerate(list_):
		if val is None:
			continue
		list_[idx] = Node(val)
		if idx == 0:
			continue
		parent, lr_idx = divmod(idx - 1, 2)
		if lr_idx == 0:
			list_[parent].left = list_[idx]
		else:  # lr_idx == 1:
			list_[parent].right = list_[idx]
	return list_[0]


#	   0
#	 1   2
#	3 4 5 6
tree = list_to_tree(list(range(7)))
print(Solution().connect(tree).val)
"""
