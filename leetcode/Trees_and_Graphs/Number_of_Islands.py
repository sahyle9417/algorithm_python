from collections import deque
from typing import List


dr_list = [0, 0, -1, 1]
dc_list = [-1, 1, 0, 0]


class Solution:
	def numIslands(self, grid: List[List[str]]) -> int:
		m = len(grid)
		n = len(grid[0])

		visited = [[False] * n for _ in range(m)]

		island_num = 0

		for row in range(m):
			for col in range(n):

				if grid[row][col] == "0":
					continue
				if visited[row][col]:
					continue

				island_num += 1

				q = deque([[row, col]])

				while q:
					r, c = q.popleft()
					visited[r][c] = True

					for dr, dc in zip(dr_list, dc_list):
						next_r = r + dr
						next_c = c + dc
						if next_r < 0 or next_r >= m:
							continue
						if next_c < 0 or next_c >= n:
							continue
						if grid[next_r][next_c] == "0":
							continue
						if visited[next_r][next_c]:
							continue

						# 이거 없으면 시간 초과 (동일 좌표가 중복 처리되기 때문)
						visited[next_r][next_c] = True
						q.append([next_r, next_c])

		return island_num


"""
grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
print(Solution().numIslands(grid))

grid = [
	["1", "1", "0", "0", "0"],
	["1", "1", "0", "0", "0"],
	["0", "0", "1", "0", "0"],
	["0", "0", "0", "1", "1"],
]
print(Solution().numIslands(grid))
"""
