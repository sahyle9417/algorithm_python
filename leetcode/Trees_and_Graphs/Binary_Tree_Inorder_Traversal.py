from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:
	# Iterative Approach
	def inorderTraversal(self, root: TreeNode) -> List[int]:
		answer = []
		stack = []
		node = root

		while stack or node:

			# 왼쪽으로 이동 반복
			if node:
				stack.append(node)
				node = node.left

			# 왼쪽 자식 없다면 부모 처리 후 오른쪽 서브트리로 이동
			else:
				node = stack.pop()
				answer.append(node.val)
				node = node.right

		return answer

	# Recursive Approach
	def inorderTraversalRecursive(self, root: TreeNode) -> List[int]:
		if root is None:
			return []
		left = self.inorderTraversalRecursive(root.left)
		right = self.inorderTraversalRecursive(root.right)
		return left + [root.val] + right


"""
def list_to_tree(list_: List[int]) -> List[TreeNode]:
	for idx, val in enumerate(list_):
		if val is None:
			continue
		list_[idx] = TreeNode(val)
		if idx == 0:
			continue
		parent, lr_idx = divmod(idx - 1, 2)
		if lr_idx == 0:
			list_[parent].left = list_[idx]
		else:  # lr_idx == 1:
			list_[parent].right = list_[idx]
	return list_[0]


tree = list_to_tree([1, None, 2, None, None, 3])
print(Solution().inorderTraversal(tree))
"""
