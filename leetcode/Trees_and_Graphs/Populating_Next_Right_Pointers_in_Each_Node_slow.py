from collections import deque
from typing import List, Deque


# Definition for a Node.
class Node:
	def __init__(
		self,
		val: int = 0,
		left: "Node" = None,
		right: "Node" = None,
		next: "Node" = None,
	):
		self.val = val
		self.left = left
		self.right = right
		self.next = next


class Solution:
	def connect(self, root: "Node") -> "Node":
		if root is None:
			return None

		parents = deque()
		root = self.recursive_connect(root, parents)
		return root

	def recursive_connect(self, node: "Node", parents: Deque["Node"]):
		node.next = self.get_next(node, parents)

		parents.appendleft(node)

		if node.left:
			self.recursive_connect(node.left, parents)
		if node.right:
			self.recursive_connect(node.right, parents)

		parents.popleft()

		return node

	def get_next(self, src_node: "Node", parents: List["Node"]) -> "Node":
		has_next = False
		node = src_node

		for dist, parent in enumerate(parents, 1):
			if parent.left == node:
				has_next = True
				break
			node = parent

		if not has_next:
			return None

		dst_node = parent.right
		for _ in range(dist - 1):
			dst_node = dst_node.left

		return dst_node


"""
def list_to_tree(list_: List[int]) -> List[Node]:
	for idx, val in enumerate(list_):
		if val is None:
			continue
		list_[idx] = Node(val)
		if idx == 0:
			continue
		parent, lr_idx = divmod(idx - 1, 2)
		if lr_idx == 0:
			list_[parent].left = list_[idx]
		else:  # lr_idx == 1:
			list_[parent].right = list_[idx]
	return list_[0]


#	   0
#	 1   2
#	3 4 5 6
tree = list_to_tree(list(range(7)))
print(Solution().connect(tree).val)
"""
