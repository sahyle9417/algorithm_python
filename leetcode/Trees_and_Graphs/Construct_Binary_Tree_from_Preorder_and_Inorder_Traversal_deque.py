from collections import deque
from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:

	def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:

		def recursive_build(start_idx, end_idx):

			if start_idx > end_idx:
				return None

			# 재귀호출 시 무조건 preorder의 맨 왼쪽부터 처리되며
			# 맨 왼쪽에 무조건 root의 value가 들어있음
			root_val = preorder.popleft()
			root = TreeNode(root_val)
			root_idx = val_to_idx[root_val]  # inorder idx of root

			root.left = recursive_build(start_idx, root_idx - 1)
			root.right = recursive_build(root_idx + 1, end_idx)

			return root

		# 값(val)을 inorder idx로 변환
		# 이 부분 때문에 unique일 때만 가능
		val_to_idx = dict()
		for idx, val in enumerate(inorder):
			val_to_idx[val] = idx

		# popleft를 쓰기 위해 deque으로 변환
		preorder = deque(preorder)

		return recursive_build(0, len(preorder) - 1)


"""
Solution().buildTree(preorder=[3, 9, 20, 15, 7], inorder=[9, 3, 15, 20, 7])
Solution().buildTree(preorder=[1, 2, 3], inorder=[2, 3, 1])
"""
