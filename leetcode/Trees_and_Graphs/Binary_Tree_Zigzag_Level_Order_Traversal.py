from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:
	def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
		if root is None:
			return []

		nodes = [root]
		answer = []
		reverse = False

		while nodes:
			vals = [node.val for node in nodes]
			if reverse:
				vals = list(reversed(vals))
			answer.append(vals)
			reverse = not reverse
			nodes = self.get_next_level(nodes)

		return answer

	def get_next_level(self, current):
		next = []
		for node in current:
			next.extend([node.left, node.right])
		next = [node for node in next if node is not None]
		return next


"""
def list_to_tree(list_: List[int]) -> List[TreeNode]:
	for idx, val in enumerate(list_):
		if val is None:
			continue
		list_[idx] = TreeNode(val)
		if idx == 0:
			continue
		parent, lr_idx = divmod(idx - 1, 2)
		if lr_idx == 0:
			list_[parent].left = list_[idx]
		else:  # lr_idx == 1:
			list_[parent].right = list_[idx]
	return list_[0]


tree = list_to_tree([3, 9, 20, None, None, 15, 7])
tree = list_to_tree(list(range(10)))
	0
  1   2
 3 4 5 6
78 9
print(Solution().zigzagLevelOrder(tree))
"""