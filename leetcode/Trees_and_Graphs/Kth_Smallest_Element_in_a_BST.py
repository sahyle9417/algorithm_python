from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:
	def kthSmallest(self, root: TreeNode, k: int) -> int:
		stack = []
		node = root

		while True:

			# 왼쪽으로 이동 반복
			if node:
				stack.append(node)
				node = node.left

			# 왼쪽 자식 없다면 부모 처리 후 오른쪽 서브트리로 이동
			else:
				node = stack.pop()
				if k == 1:
					return node.val
				k -= 1
				node = node.right


"""
def list_to_tree(list_: List[int]) -> List[TreeNode]:
	for idx, val in enumerate(list_):
		if val is None:
			continue
		list_[idx] = TreeNode(val)
		if idx == 0:
			continue
		parent, lr_idx = divmod(idx - 1, 2)
		if lr_idx == 0:
			list_[parent].left = list_[idx]
		else:  # lr_idx == 1:
			list_[parent].right = list_[idx]
	return list_[0]


#	   3
#	 1   4
#	X 2
tree = list_to_tree([3, 1, 4, None, 2])
print(Solution().kthSmallest(tree, 1))
#	 5
#   3   6
#  2 4 X X
# 1
tree = list_to_tree([5, 3, 6, 2, 4, None, None, 1])
print(Solution().kthSmallest(tree, 3))
"""
