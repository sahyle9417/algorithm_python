from collections import deque
from typing import List


# Definition for a binary tree node.
class TreeNode:
	def __init__(self, val=0, left=None, right=None):
		self.val = val
		self.left = left
		self.right = right


class Solution:
	def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:

		answer = []

		def recursion(node, level, dir):

			if node is None:
				return
			if level >= len(answer):
				answer.append(deque())
			if dir == 0:
				answer[level].append(node.val)
			else:
				answer[level].appendleft(node.val)
			recursion(node.left, level + 1, ~dir)
			recursion(node.right, level + 1, ~dir)

		recursion(root, 0, 0)
		return answer
