from collections import deque
from typing import List, Deque


# Definition for a Node.
class Node:
	def __init__(
		self,
		val: int = 0,
		left: "Node" = None,
		right: "Node" = None,
		next: "Node" = None,
	):
		self.val = val
		self.left = left
		self.right = right
		self.next = next


class Solution:
	def connect(self, root: 'Node') -> 'Node':
		if not root:
			return None

		queue = deque()
		queue.append([root, 0])
		prev_node, prev_depth = None, -1

		while queue:
			curr_node, curr_depth = queue.popleft()

			# 이전 노드와 현재 노드가 동일 깊이라면 next로 연결
			if prev_depth == curr_depth:
				prev_node.next = curr_node

			if curr_node.left:
				queue.append([curr_node.left, curr_depth + 1])

			if curr_node.right:
				queue.append([curr_node.right, curr_depth + 1])

			prev_node, prev_depth = curr_node, curr_depth

		return root


"""
def list_to_tree(list_: List[int]) -> List[Node]:
	for idx, val in enumerate(list_):
		if val is None:
			continue
		list_[idx] = Node(val)
		if idx == 0:
			continue
		parent, lr_idx = divmod(idx - 1, 2)
		if lr_idx == 0:
			list_[parent].left = list_[idx]
		else:  # lr_idx == 1:
			list_[parent].right = list_[idx]
	return list_[0]


#	   0
#	 1   2
#	3 4 5 6
tree = list_to_tree(list(range(7)))
print(Solution().connect(tree).val)
"""
