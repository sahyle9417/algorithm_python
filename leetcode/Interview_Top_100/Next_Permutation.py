from typing import List
from bisect import bisect

class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        turn_idx = None

        # 뒤에서부터 순회
        for idx in range(len(nums) - 1, 0, -1):
            # 내림차순 정렬 깨지는 최초 지점(변곡점) 찾기 (같아도 스킵)
            if nums[idx - 1] < nums[idx]:
                turn_idx = idx
                break
        # 변곡점 없음 = 전체가 내림차순 정렬 = next perm 없음
        # for-else 사용 가능 (break 없이 for문 전체 순회 시 else 진입) 
        if turn_idx is None:
            nums.sort()
            return

        # 내림차순 정렬된 뒷부분을 거꾸로 뒤집어서 오름차순 정렬 만듦
        nums[turn_idx:] = nums[turn_idx:][::-1]

        # 오름차순 정렬된 뒷부분에서 변곡점보다 큰 최솟값 위치(swap_idx) 찾음
        swap_idx = bisect(nums, nums[turn_idx - 1], lo=turn_idx)
        # 두 값이 동일하다면 swap 무의미 -> 다음으로 큰 값으로 변경
        if nums[turn_idx - 1] == nums[swap_idx]:
            swap_idx += 1

        # 변곡점과 변곡점보다 큰 최솟값을 swap
        nums[turn_idx - 1], nums[swap_idx] = nums[swap_idx], nums[turn_idx - 1]

        return


"""Solution().nextPermutation([2, 3, 1])
Solution().nextPermutation([1, 2, 3, 4, 5])
Solution().nextPermutation([5, 4, 3, 2, 1])
Solution().nextPermutation([3, 2, 1])
Solution().nextPermutation([1, 4, 3, 2])
Solution().nextPermutation([1, 4, 3, 3, 2])
Solution().nextPermutation([1, 5, 1])
Solution().nextPermutation([2,2,7,5,4,3,2,2,1])  # [2,3,1,2,2,2,4,5,7]
"""
