from bisect import insort, bisect_left
from collections import defaultdict
from typing import List


class TweetCounts:

    def __init__(self):
        self.get_times = defaultdict(list)
        self.get_interval = {
            "minute": 60,
            "hour": 3600,
            "day": 86400
        }

    def recordTweet(self, tweetName: str, time: int) -> None:
        insort(self.get_times[tweetName], time)

    def getTweetCountsPerFrequency(self, freq: str, tweetName: str, startTime: int, endTime: int) -> List[int]:
        answer = []

        interval = self.get_interval[freq]
        times = self.get_times[tweetName]

        for interval_start in range(startTime, endTime + 1, interval):
            # interval_end는 실제로 interval에 포함X
            interval_end = interval_start + interval
            interval_end = min(interval_end, endTime + 1)

            # idx_end는 실제로 interval에 포함X
            idx_start = bisect_left(times, interval_start)
            idx_end = bisect_left(times, interval_end)

            answer.append(idx_end - idx_start)

        return answer


opers = [
	"recordTweet",
	"recordTweet",
	"recordTweet",
	"getTweetCountsPerFrequency",
	"getTweetCountsPerFrequency",
	"recordTweet",
	"getTweetCountsPerFrequency"
]
params = [
	["tweet3",0],
	["tweet3",60],
	["tweet3",10],
	["minute","tweet3",0,59],
	["minute","tweet3",0,60],
	["tweet3",120],
	["hour","tweet3",0,210]
]
solution = TweetCounts()
for oper, param in zip(opers, params):
    if oper == "recordTweet":
        tweetName, time = param
        solution.recordTweet(tweetName, time)
    if oper == "getTweetCountsPerFrequency":
        freq, tweetName, startTime, endTime = param
        answer = solution.getTweetCountsPerFrequency(freq, tweetName, startTime, endTime)
        print(answer)

# Your TweetCounts object will be instantiated and called as such:
# obj = TweetCounts()
# obj.recordTweet(tweetName,time)
# param_2 = obj.getTweetCountsPerFrequency(freq,tweetName,startTime,endTime)