from typing import List


class Solution:

    def rotate_layer(self, matrix, start, end):
        for idx in range(end - start):
            tmp = matrix[start][start + idx]
            matrix[start][start + idx] = matrix[end - idx][start]
            matrix[end - idx][start] = matrix[end][end - idx]
            matrix[end][end - idx] = matrix[start + idx][end]
            matrix[start + idx][end] = tmp

    def rotate(self, matrix: List[List[int]]) -> None:
        for layer_idx in range(len(matrix) // 2):
            start = layer_idx
            end = len(matrix) - layer_idx - 1
            self.rotate_layer(matrix, start, end)


# matrix = [[1,2,3],[4,5,6],[7,8,9]]
# Solution().rotate(matrix)
# print(matrix)

# matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
# Solution().rotate(matrix)
# print(matrix)
