from collections import Counter
from typing import List


class Solution:
    def deleteAndEarn(self, nums: List[int]) -> int:
        ctr = Counter(nums)

        # 바로 직전 반복에서의 curr
        prev = None

        # curr를 사용(명시적으로 삭제)할 경우 획득 가능한 최대 포인트
        avoid = 0

        # curr를 사용(명시적으로 삭제)하지 않을 경우 획득 가능한 최대 포인트
        using = 0

        # 획득 가능한 최대 포인트
        answer = 0

        for curr in sorted(ctr):

            # prev와 curr이 인접하지 않아서 둘 다 사용 가능
            if curr - 1 != prev:
                # using : 기존 최대 포인트 + curr로 획득 가능한 포인트
                using = answer + curr * ctr[curr]
                # avoid : 기존 최대 포인트 (curr로 포인트 획득 불가)
                avoid = answer

            # prev와 curr이 인접하여 둘 다 사용 불가
            else:
                # using : prev 미사용 시 획득 가능한 최대 포인트 + curr로 획득 가능한 포인트
                # (curr이 아니라) prev 미사용 시의 최대 포인트를 쓰기 때문에
                # curr에 대한 avoid 값이 세팅되기 전에 prev의 avoid 값 사용
                using = avoid + curr * ctr[curr]
                # avoid : 기존 최대 포인트 (curr로 포인트 획득 불가)
                avoid = answer

            answer = max(avoid, using)
            prev = curr

        return answer


"""
print(f"answer:{Solution().deleteAndEarn([3,4,2])}\n")
print(f"answer:{Solution().deleteAndEarn([2,2,3,3,3,4])}\n")
print(f"answer:{Solution().deleteAndEarn([8,7,3,8,1,4,2])}\n")
print(f"answer:{Solution().deleteAndEarn([8,7,3,8,1,4,10,10,10,2])}\n")
print(f"answer:{Solution().deleteAndEarn([1,2,3,4,7,8,8,10,10,10])}\n")
"""
