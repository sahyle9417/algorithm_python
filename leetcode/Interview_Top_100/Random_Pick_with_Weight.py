from bisect import bisect
from collections import Counter
from random import randint


class Solution(object):

    def __init__(self, w):
        # cumulative weight
        self.cum_w = w.copy()
        for idx in range(1, len(w)):
            self.cum_w[idx] += self.cum_w[idx - 1]

    def pickIndex(self):
        # get target number from range 0 ~ sum of weight
        target = randint(0, self.cum_w[-1] - 1)
        # find idx of target number
        return bisect(self.cum_w, target)


"""
ctr = Counter()
for _ in range(1000):
    num = Solution([1,3]).pickIndex()
    ctr[num] += 1
print(ctr)
"""
