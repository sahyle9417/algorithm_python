class Solution:
    def numDecodings(self, s: str) -> int:
        if s == '0':
            return 0
        
        length = len(s)
        dp = [0] * (length + 1)
        dp[length] = 1
        dp[length - 1] = 0 if s[-1] == '0' else 1
        
        valid = set(map(str, range(1, 27)))

        for idx in range(length-2, -1, -1):
            cnt = 0

            # 마지막 한글자 분리하는 경우
            if s[idx] in valid:
                cnt += dp[idx + 1]

            # 마지막 두글자 분리하는 경우
            if s[idx: idx+2] in valid:
                cnt += dp[idx + 2]

            dp[idx] = cnt

        return dp[0]


"""
print(Solution().numDecodings("123"))
print(Solution().numDecodings("226"))
print(Solution().numDecodings("220"))
print(Solution().numDecodings("06"))
print(Solution().numDecodings("1201234"))
print(Solution().numDecodings("1234"))
"""
