from collections import deque


class Solution:
    def decodeString(self, s: str) -> str:
        cnt = 0
        string = ""

        cnt_stack = deque()
        str_stack = deque()

        for c in s:

            # get cnt
            if c.isdigit():
                cnt = cnt * 10 + int(c)

            # append to stack and initialize variable
            elif c == "[":
                cnt_stack.append(cnt)
                str_stack.append(string)
                cnt = 0
                string = ""

            # append to curr_str
            elif c.isalpha():
                string += c

            # update str
            elif c == "]":
                pre_str = str_stack.pop()
                curr_cnt = cnt_stack.pop()
                string = pre_str + curr_cnt * string

        return string


# print(Solution().decodeString("3[a2[c]]"))
