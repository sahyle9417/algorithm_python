from collections import deque
from typing import Deque, List


class Solution:

    def get_equation_from_str(self, s: str) -> Deque:
        equation = deque()

        start_idx = 0
        while start_idx < len(s):

            # append operand
            end_idx = start_idx
            while end_idx < len(s):
                if s[end_idx] in ["*", "/", "+", "-"]:
                    break
                else:
                    end_idx += 1
            equation.append(int(s[start_idx:end_idx]))

            # append operator (if exists)
            if end_idx < len(s):
                equation.append(s[end_idx])
            start_idx = end_idx + 1

        return equation

    def remove_operator(self, equation: Deque, operator: List) -> Deque:
        new_equation = deque()
        while equation:
            curr = equation.popleft()
            if curr in operator:
                prev = new_equation.pop()
                next = equation.popleft()
                if curr == "*":
                    new_equation.append(prev * next)
                if curr == "/":
                    new_equation.append(prev // next)
                if curr == "+":
                    new_equation.append(prev + next)
                else: # if curr == "-":
                    new_equation.append(prev - next)
            else:
                new_equation.append(curr)
        return new_equation

    def calculate(self, s: str) -> int:
        equation = self.get_equation_from_str(s)
        equation = self.remove_operator(equation, ["*", "/"])
        equation = self.remove_operator(equation, ["+", "-"])
        return equation.pop()


"""
print(Solution().calculate("3+2*2"))
print(Solution().calculate(" 3/2 "))
"""
