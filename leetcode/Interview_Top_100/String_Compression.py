from math import log10
from typing import List


class Solution:
    def compress(self, chars: List[str]) -> int:
        prev = chars[0]
        cnt = 1

        r_idx = 1
        w_idx = 1

        while r_idx < len(chars):
            # read char
            curr = chars[r_idx]
            r_idx += 1

            # found same char
            if prev == curr:
                cnt += 1
            # found diff char
            else:
                if cnt > 1:
                    cnt_len = int(log10(cnt) + 1)
                    chars[w_idx : w_idx + cnt_len] = list(str(cnt))
                    w_idx += cnt_len

                # write char
                chars[w_idx] = curr
                w_idx += 1

                prev = curr
                cnt = 1

        cnt_len = 0
        if cnt > 1:
            cnt_len = int(log10(cnt) + 1)
            chars[w_idx : w_idx + cnt_len] = list(str(cnt))

        del chars[w_idx + cnt_len:]
        return len(chars)


"""
print(Solution().compress(["a","a","b","b","c","c","c"]))
print(Solution().compress(["a"]))
print(Solution().compress(["a","b","b","b","b","b","b","b","b","b","b","b","b"]))
print(Solution().compress(["a","a","a","b","b","a","a"]))
print(Solution().compress(["a","a","a","a","a","b"]))
print(Solution().compress(["v","r","r","r","r","r","r","r","r","r"]))
"""
