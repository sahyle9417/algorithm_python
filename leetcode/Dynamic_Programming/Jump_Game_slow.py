from typing import List


class Solution:
	def canJump(self, nums: List[int]) -> bool:
		distance = 0

		for num in nums[:-1]:
			if distance == 0 and num == 0:
				return False
			distance = max(distance, num) - 1

		return True


"""
print(Solution().canJump([2, 3, 1, 1, 4]))
print(Solution().canJump([3, 2, 1, 0, 4]))
"""
