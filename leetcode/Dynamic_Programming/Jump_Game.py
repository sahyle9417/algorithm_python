from typing import List


class Solution:
	def canJump(self, nums: List[int]) -> bool:
		reachable = len(nums) - 1		
		for i in range(len(nums) - 2, -1, -1):
			if i + nums[i] >= reachable:
				reachable = i
		return reachable == 0


"""
print(Solution().canJump([2, 3, 1, 1, 4]))
print(Solution().canJump([3, 2, 1, 0, 4]))
"""
