from collections import deque
from typing import List


class Solution:
	def coinChange(self, coins: List[int], amount: int) -> int:
		q = deque([0])
		answer = 0
		visited = {0}

		coins = sorted(coins)

		while q:

			if min(q) > amount:
				return -1

			q_len = len(q)
			for _ in range(q_len):
				money = q.popleft()
				if money == amount:
					return answer
				for coin in coins:
					if money + coin > amount:
						continue
					if money + coin in visited:
						continue
					q.append(money + coin)
					visited.add(money + coin)
			answer += 1

		return -1


"""
print(Solution().coinChange(coins=[1, 2, 5], amount=11))
print(Solution().coinChange(coins=[2], amount=3))
print(Solution().coinChange(coins=[1], amount=0))
print(Solution().coinChange(coins=[1], amount=1))
print(Solution().coinChange(coins=[1], amount=2))
print(Solution().coinChange(coins=[1, 2147483647], amount=2))
print(Solution().coinChange(coins=[1,2,5], amount=100))
"""
