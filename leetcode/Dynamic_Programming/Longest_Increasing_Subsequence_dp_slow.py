from typing import List


class Solution:
	def lengthOfLIS(self, nums: List[int]) -> int:
		# dp[i]: nums[i]로 끝나는 LIS의 길이
		# dp[i]: the length of LIS ends with nums[i]
		dp = []

		for i, tail in enumerate(nums):
			# LIS 최소 길이는 1
			dp.append(1)

			# tail을 가장 긴 seq의 뒤에 붙임
			for j in range(i):
				if nums[j] < tail:
					dp[i] = max(dp[i], dp[j] + 1)

		return max(dp) if dp else 0


"""
print(Solution().lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]))
print(Solution().lengthOfLIS([0, 1, 0, 3, 2, 3]))
print(Solution().lengthOfLIS([0, 2, 3, 1]))
print(Solution().lengthOfLIS([7, 7, 7, 7, 7, 7, 7]))
print(Solution().lengthOfLIS(list(range(1000))))
"""
