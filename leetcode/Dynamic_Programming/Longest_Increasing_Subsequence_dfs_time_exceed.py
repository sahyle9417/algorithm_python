import math
from typing import List


class Solution:
	def lengthOfLIS(self, nums: List[int]) -> int:
		self.answer = 0
		self.length = len(nums)

		def dfs(idx, lis_length, prev_max):

			while idx < self.length and nums[idx] <= prev_max:
				idx += 1

			if idx == self.length:
				return

			# 기존 max보다 큰 값 발견
			self.answer = max(self.answer, lis_length + 1)

			# 기존 seq 연장
			dfs(idx + 1, lis_length + 1, nums[idx])

			# 기존 seq 유지
			dfs(idx + 1, lis_length, prev_max)

		dfs(idx=0, lis_length=0, prev_max=-math.inf)

		return self.answer


"""
print(Solution().lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]))
print(Solution().lengthOfLIS([0, 1, 0, 3, 2, 3]))
print(Solution().lengthOfLIS([0, 2, 3, 1]))
print(Solution().lengthOfLIS([7, 7, 7, 7, 7, 7, 7]))
print(Solution().lengthOfLIS(list(range(1000))))
"""
