import math
from typing import List


class Solution:
	def coinChange(self, coins: List[int], amount: int) -> int:
		dp = [math.inf] * (amount + 1)
		dp[0] = 0

		for coin in coins:
			for idx in range(coin, amount + 1):
				# 파이썬은 math.inf+1이 math.inf와 같아서 dp[idx] + 1 해도 됨
				# 자바에서는 Integer.MAX_VALUE + 1은 오버플로우 발생해서 음수되므로 안됨
				dp[idx] = min(dp[idx], dp[idx - coin] + 1)

		if dp[amount] == math.inf:
			return -1
		else:
			return dp[amount]


# print(Solution().coinChange(coins=[1, 2, 5], amount=11))

