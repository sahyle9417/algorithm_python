class Solution:
	def uniquePaths(self, m: int, n: int) -> int:
		# 각 row의 dp 결과 기록
		# 초기엔 1번째 row의 dp 결과 저장
		dp = [1] * n

		# 매 반복마다 다음 row의 dp 결과로 변경
		for _ in range(1, m):
			for col in range(1, n):
				# 위쪽 칸 값에 왼쪽 칸의 값을 더함
				dp[col] += dp[col - 1]

		# 마지막 row의 마지막 col의 dp 결과 반환
		return dp[-1]


"""
print(Solution().uniquePaths(m = 7, n = 3))
"""
