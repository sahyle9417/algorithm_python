from math import factorial


class Solution:
	def uniquePaths(self, m: int, n: int) -> int:
		# ((m-1)+(n-1)) C (m-1)
		return int(factorial(m + n - 2) / (factorial(m - 1) * factorial(n - 1)))


"""
print(Solution().uniquePaths(m = 7, n = 3))
"""
