from bisect import bisect_left
from typing import List


class Solution:
	def lengthOfLIS(self, nums: List[int]) -> int:
		LIS = []

		for num in nums:

			# 나보다 크거나 같은 숫자 중 최소 인덱스 찾기
			# Find the first index "left" which satisfies LIS[left] >= num
			idx = bisect_left(LIS, num)

			# 나보다 크거나 같은 숫자 중 최소 인덱스 갱신
			# 갱신하다보면 유효하지 않은 LIS가 나올 수 있는데 길이는 맞음
			# 예를 들어 [0, 2, 3, 1] 의 경우 LIS는 [0, 1, 3] 가 된다
			# 하지만 궁극적으로 LIS 길이는 맞게 나오니까 상관없음
			if idx < len(LIS):
				LIS[idx] = num
			# 나보다 크거나 같은 숫자가 없다면 나를 맨뒤에 추가
			else:  # idx == len(LIS)
				LIS.append(num)

		return len(LIS)


"""
print(Solution().lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]))
print(Solution().lengthOfLIS([0, 1, 0, 3, 2, 3]))
print(Solution().lengthOfLIS([0, 2, 3, 1]))
print(Solution().lengthOfLIS([7, 7, 7, 7, 7, 7, 7]))
print(Solution().lengthOfLIS(list(range(1000))))
"""
