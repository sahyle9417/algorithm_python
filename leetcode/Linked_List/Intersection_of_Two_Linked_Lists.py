# Definition for singly-linked list.
class ListNode:
	def __init__(self, x):
		self.val = x
		self.next = None


class Solution:

	def get_length(self, head):
		length = 0
		while head:
			length += 1
			head = head.next
		return length

	def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
		lenA = self.get_length(headA)
		lenB = self.get_length(headB)
		while lenA < lenB:
			headB = headB.next
			lenB -= 1
		while lenB < lenA:
			headA = headA.next
			lenA -= 1
		while headA != headB:
			headA = headA.next
			headB = headB.next
		return headA


"""
def arr_to_list(arr):
	head = tail = None
	for num in arr:
		if head is None:
			head = tail = ListNode(num)
		else:
			tail.next = ListNode(num)
			tail = tail.next
	return head, tail


def print_list(l: ListNode):
	ret = []
	while l:
		ret.append(l.val)
		l = l.next
	print(ret)


head1, tail1 = arr_to_list([4, 1])
head2, tail2 = arr_to_list([5, 6, 1])
head3, _ = arr_to_list([8, 4, 5])
tail1.next = head3
tail2.next = head3
print_list(Solution().getIntersectionNode(head1, head2))
"""
