# Definition for singly-linked list.
class ListNode:
	def __init__(self, val=0, next=None):
		self.val = val
		self.next = next


class Solution:
	def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
		# 더미노드 추가
		head = ListNode(0)
		tail = head
		carry = 0

		# l1, l2, carry 중 하나라도 남아있는 동안 반복
		while l1 or l2 or carry:
			val1 = l1.val if l1 else 0
			val2 = l2.val if l2 else 0
			carry, remain = divmod(val1 + val2 + carry, 10)

			tail.next = ListNode(remain)
			tail = tail.next

			l1 = l1.next if l1 else None
			l2 = l2.next if l2 else None

		# l1, l2는 None이고 carry는 0인 상태 도달
		# 맨 앞의 더미노드 제외하고 반환
		return head.next


"""
def arr_to_list(arr):
	head = None
	tail = None
	for num in arr:
		if head is None:
			head = tail = ListNode(num)
		else:
			tail.next = ListNode(num)
			tail = tail.next
	return head


arr1 = [9, 9, 9, 9, 9, 9, 9]
arr2 = [9, 9, 9, 9]
l1 = arr_to_list(arr1)
l2 = arr_to_list(arr2)
l = Solution().addTwoNumbers(l1, l2)
while l:
	print(l.val)
	l = l.next
"""
