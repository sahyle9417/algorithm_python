# Definition for singly-linked list.
class ListNode:
	def __init__(self, val=0, next=None):
		self.val = val
		self.next = next


class Solution:
	def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
		# 반환할 리스트
		head = None
		tail = None

		# 윗자리 올림수
		carry = 0

		# l1과 l2에 모두 노드가 있음
		while l1 is not None and l2 is not None:
			carry, remain = divmod(l1.val + l2.val + carry, 10)
			if head is None:
				head = tail = ListNode(remain)
			else:
				tail.next = ListNode(remain)
				tail = tail.next
			l1 = l1.next
			l2 = l2.next

		# l1, l2 중 하나에만 노드가 있음
		for l in [l1, l2]:
			while l is not None:
				carry, remain = divmod(l.val + carry, 10)
				tail.next = ListNode(remain)
				tail = tail.next
				l = l.next

		# l1, l2 모두 끝나고 남은 carry 처리
		if carry != 0:
			tail.next = ListNode(carry)
			tail = tail.next

		return head


"""
def arr_to_list(arr):
	head = None
	tail = None
	for num in arr:
		if head is None:
			head = tail = ListNode(num)
		else:
			tail.next = ListNode(num)
			tail = tail.next
	return head


arr1 = [9, 9, 9, 9, 9, 9, 9]
arr2 = [9, 9, 9, 9]
l1 = arr_to_list(arr1)
l2 = arr_to_list(arr2)
l = Solution().addTwoNumbers(l1, l2)
while l:
	print(l.val)
	l = l.next
"""
