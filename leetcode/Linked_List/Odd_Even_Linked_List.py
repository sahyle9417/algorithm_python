# Definition for singly-linked list.
class ListNode:
	def __init__(self, val=0, next=None):
		self.val = val
		self.next = next


class Solution:
	def oddEvenList(self, head: ListNode) -> ListNode:
		if not head:
			return None

		odd_head = odd_tail = head
		even_head = even_tail = head.next

		while even_tail and even_tail.next:
			# 다다음 노드를 현재 노드의 next로 연결
			odd_tail.next = odd_tail.next.next
			even_tail.next = even_tail.next.next
			# 위에서 연결한 다음(구 다다음) 노드로 이동
			odd_tail = odd_tail.next
			even_tail = even_tail.next

		# 홀수 리스트 뒤에 짝수 리스트 연결
		odd_tail.next = even_head

		return odd_head


def arr_to_list(arr):
	head = None
	tail = None
	for num in arr:
		if head is None:
			head = tail = ListNode(num)
		else:
			tail.next = ListNode(num)
			tail = tail.next
	return head


arr = [2, 1, 3, 5, 6, 4, 7]
l = arr_to_list(arr)
l = Solution().oddEvenList(l)
while l:
	print(l.val)
	l = l.next
