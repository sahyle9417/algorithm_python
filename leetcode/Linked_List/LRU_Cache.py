from collections import OrderedDict


class LRUCache:

	def __init__(self, capacity: int):
		self.capacity = capacity
		self.size = 0
		self.hash_table = OrderedDict()

	def get(self, key: int) -> int:
		if key not in self.hash_table:
			return -1
		#value = self.hash_table.pop(key)
		#self.hash_table[key] = value
		self.hash_table.move_to_end(key)
		return self.hash_table[key]

	def put(self, key: int, value: int) -> None:
		if key in self.hash_table:
			self.hash_table.move_to_end(key)
		elif self.size == self.capacity:
			self.hash_table.popitem(last=False)
		else:
			self.size += 1
		self.hash_table[key] = value


"""
# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
obj = LRUCache(2)
obj.put(1,1)
obj.put(2,2)
print(obj.get(1))
obj.put(3,3)
print(obj.get(2))
obj.put(4,4)
print(obj.get(1))
print(obj.get(3))
print(obj.get(4))
"""
