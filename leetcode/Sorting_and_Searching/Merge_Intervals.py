from typing import List


class Solution:
	def merge(self, intervals: List[List[int]]) -> List[List[int]]:
		intervals = sorted(intervals, key=lambda x: x[0])

		answer = []

		before = intervals[0]

		for current in intervals[1:]:
			if before[1] >= current[0]:
				before[1] = max(before[1], current[1])
			else:
				answer.append(before)
				before = current

		answer.append(before)

		return answer


"""
print(Solution().merge([[1, 3], [2, 6], [8, 10], [15, 18]]))
print(Solution().merge([[1, 4], [4, 5]]))
"""
