import math
from typing import List


class Solution:
	def findPeakElement(self, nums: List[int]) -> int:
		"""
		left와 right 사이에 Peak이 존재하게끔 범위를 좁혀나가면 된다.
		문제 조건에 따라 이웃되는 수는 같지 않으므로 감소하거나 증가한다.
		따라서 mid와 바로 다음 수를 비교할 때
		증가한다면 mid의 오른쪽에 Peak가 존재하고
		감소한다면 왼쪽에 Peak가 존재한다.
		"""
		left = 0
		right = len(nums) - 1

		while left < right:
			mid = (left + right) // 2
			if nums[mid] < nums[mid + 1]:
				left = mid + 1
			else:
				right = mid

		return left


"""
print(Solution().findPeakElement([1, 2, 1, 3, 5, 6, 4]))
print(Solution().findPeakElement([1, 2, 3, 1]))
"""
