import math
from typing import List


class Solution:
	def findPeakElement(self, nums: List[int]) -> int:
		before = -math.inf
		nums.append(-math.inf)

		for idx, num in enumerate(nums[:-1]):
			if before < num and nums[idx + 1] < num:
				return idx
			before = num


"""
print(Solution().findPeakElement([1, 2, 1, 3, 5, 6, 4]))
print(Solution().findPeakElement([1, 2, 3, 1]))
"""
