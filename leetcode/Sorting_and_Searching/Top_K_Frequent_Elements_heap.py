from collections import Counter
from heapq import nlargest
from typing import List


class Solution:
	def topKFrequent(self, nums: List[int], k: int) -> List[int]:
		num_to_freq = Counter(nums)
		return nlargest(k, num_to_freq, key=num_to_freq.get)


"""
nums = [1, 1, 1, 2, 2, 3]
k = 2
print(Solution().topKFrequent(nums, k))
"""
