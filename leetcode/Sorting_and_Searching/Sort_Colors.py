from collections import Counter
from typing import List


class Solution:
	def sortColors(self, nums: List[int]) -> None:
		"""
		Do not return anything, modify nums in-place instead.
		"""
		counter = Counter(nums)
		idx = 0

		for num in range(3):
			for _ in range(counter[num]):
				nums[idx] = num
				idx += 1


#Solution().sortColors([2,0,2,1,1,0])
