from typing import List


class Solution:
	def searchRange(self, nums: List[int], target: int) -> List[int]:
		answer = []

		# 시작지점 찾기 -> 끝지점 찾기
		for round in ["find_low", "find_high"]:
			hit = -1
			low = 0
			high = len(nums) - 1

			while low <= high:
				mid = (low + high) // 2
				if nums[mid] < target:
					low = mid + 1
				elif target < nums[mid]:
					high = mid - 1
				else:  # nums[mid] == target:
					hit = mid
					if round == "find_low":
						high = mid - 1
					else:  # find high
						low = mid + 1

			answer.append(hit)

		return answer


"""
print(Solution().searchRange(nums=[5, 7, 7, 8, 8, 10], target=8))
print(Solution().searchRange(nums=[5, 7, 7, 8, 8, 10], target=6))
print(Solution().searchRange(nums=[], target=0))
print(Solution().searchRange(nums=[1, 1, 3, 3, 4, 4], target=4))
print(Solution().searchRange(nums=[1, 1, 3, 3, 4, 4], target=1))
"""
