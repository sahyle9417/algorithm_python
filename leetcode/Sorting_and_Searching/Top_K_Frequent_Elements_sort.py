from collections import Counter
from typing import List


class Solution:
	def topKFrequent(self, nums: List[int], k: int) -> List[int]:
		num_to_freq = Counter(nums)
		num_to_freq = sorted(num_to_freq.items(), key=lambda x: -x[1])
		return [num for num, freq in num_to_freq[:k]]


"""
nums = [1, 1, 1, 2, 2, 3]
k = 2
print(Solution().topKFrequent(nums, k))
"""
