from typing import List


class Solution:
	def search(self, nums: List[int], target: int) -> int:
		left = 0
		right = len(nums) - 1

		# 모든 영역을 다 확인(left == right)할 때까지 반복
		while left <= right:
			mid = (left + right) // 2

			# hit
			if nums[mid] == target:
				return mid

			# 첫번째 요소와 target 사이에 pivot 없음 (target 우측에 pivot 있음)
			# 즉, 첫번째 요소에서 target까지의 영역은 단조증가
			if nums[0] <= target:
				# 첫번째 요소, mid번째 요소, target이 순서대로 등장
				# 즉, target이 mid 우측에 있음
				if nums[0] <= nums[mid] < target:
					left = mid + 1
				# 아래 조건 중 하나라도 맞으면 target은 mid 좌측에 있음
				# 1) nums[mid] < nums[0] : mid가 pivot 우측에 있음
				# 2) target < nums[mid] : mid가 target의 우측이자 pivot의 좌측에 있음
				else:
					right = mid - 1

			# 첫번째 요소와 target 사이에 pivot 있음 (nums[0] > target)
			else:
				# target, mid번째 요소, 첫번째 요소가 순서대로 등장
				# 즉, target이 mid 좌측에 있음
				if target < nums[mid] < nums[0]:
					right = mid - 1
				# 아래 조건 중 하나라도 맞으면 target은 mid 우측에 있음
				# 1) nums[0] <= nums[mid] : 첫번째 요소와 mid 사이에 pivot 없음
				# 2) nums[mid] < target : mid가 pivot의 우측이자 target의 좌측에 있음
				else:
					left = mid + 1

		return -1
