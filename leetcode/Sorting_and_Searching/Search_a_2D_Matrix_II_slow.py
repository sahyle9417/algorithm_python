from typing import List


class Solution:
	def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
		n = len(matrix[0])

		for row in matrix:
			if target < row[0]:
				return False

			low = 0
			high = n

			while low < high:
				mid = (low + high) // 2
				if row[mid] < target:
					low = mid + 1
				elif target < row[mid]:
					high = mid
				else:
					return True

		return False


"""
matrix = [
	[1, 4, 7, 11, 15],
	[2, 5, 8, 12, 19],
	[3, 6, 9, 16, 22],
	[10, 13, 14, 17, 24],
	[18, 21, 23, 26, 30],
]
target = 5
print(Solution().searchMatrix(matrix, target))

matrix = [
	[1, 4, 7, 11, 15],
	[2, 5, 8, 12, 19],
	[3, 6, 9, 16, 22],
	[10, 13, 14, 17, 24],
	[18, 21, 23, 26, 30],
]
target = 20
print(Solution().searchMatrix(matrix, target))
"""
