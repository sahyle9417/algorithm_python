from typing import List


class Solution:
	def searchMatrix(self, matrix, target):
		m = len(matrix)
		n = len(matrix[0])

		# 좌측 하단에서 시작
		row = m - 1
		col = 0

		while row >= 0 and col < n:

			# 감소해야됨 -> 위로 이동
			if matrix[row][col] > target:
				row -= 1
			# 증가해야됨(target row 발견) -> 우측 이동
			elif matrix[row][col] < target:
				col += 1
			else:  # hit
				return True

		return False


"""
matrix = [
	[1, 4, 7, 11, 15],
	[2, 5, 8, 12, 19],
	[3, 6, 9, 16, 22],
	[10, 13, 14, 17, 24],
	[18, 21, 23, 26, 30],
]
target = 5
print(Solution().searchMatrix(matrix, target))

matrix = [
	[1, 4, 7, 11, 15],
	[2, 5, 8, 12, 19],
	[3, 6, 9, 16, 22],
	[10, 13, 14, 17, 24],
	[18, 21, 23, 26, 30],
]
target = 20
print(Solution().searchMatrix(matrix, target))
"""
