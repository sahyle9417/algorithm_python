from typing import List


class Solution:
	def searchRange(self, nums: List[int], target: int) -> List[int]:
		# target이 존재할 수 있는 가장 낮은 위치
		low = 0
		# target이 절대 없는 가장 낮은 위치
		high = length = len(nums)

		hit = False

		while low < high:
			mid = (low + high) // 2
			if nums[mid] < target:
				low = mid + 1
			elif target < nums[mid]:
				high = mid
			else:
				hit = True
				break

		if not hit:
			return [-1, -1]

		floor = ceil = mid
		while 0 <= floor and nums[floor] == target:
			floor -= 1
		floor += 1

		while ceil < length and nums[ceil] == target:
			ceil += 1
		ceil -= 1

		return [floor, ceil]


"""
print(Solution().searchRange(nums=[5, 7, 7, 8, 8, 10], target=8))
print(Solution().searchRange(nums=[5, 7, 7, 8, 8, 10], target=6))
print(Solution().searchRange(nums=[], target=0))
print(Solution().searchRange(nums=[1, 1, 3, 3, 4, 4], target=4))
print(Solution().searchRange(nums=[1, 1, 3, 3, 4, 4], target=1))
"""
