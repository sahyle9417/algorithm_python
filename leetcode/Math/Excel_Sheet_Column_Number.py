class Solution:
	def titleToNumber(self, columnTitle: str) -> int:
		answer = 0
		for c in columnTitle:
			answer = answer * 26 + (ord(c) - ord('A') + 1)
		return answer


#print(Solution().titleToNumber("AB"))
#print(Solution().titleToNumber("ZY"))
