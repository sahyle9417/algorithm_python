class Solution:
	def fractionToDecimal(self, numerator: int, denominator: int) -> str:
		if numerator == 0:
			return "0"

		result = []

		# 결과에 부호 붙이고 나서 입력값에서 부호 제거
		if numerator < 0 and denominator > 0 or numerator >= 0 and denominator < 0:
			result.append("-")
		numerator, denominator = abs(numerator), abs(denominator)

		# 정수부 처리
		result.append(str(numerator // denominator))
		remains = numerator % denominator

		# 나머지 0이면 정수부만 반환
		if remains == 0:
			return "".join(result)

		# 소수부 시작하기 전에 소수점 추가
		result.append(".")

		# 나머지로 나온 숫자들의 인덱스를 기록하는 dict
		val_to_idx = dict()

		# 나머지가 0이 아니라면 계속 반복
		while remains:

			# 나왔던 숫자가 또 나오면 그 뒤로 반복이므로 앞뒤로 괄호 붙여서 반환
			if remains in val_to_idx:
				# insert(idx, val) : list의 특정 idx에 val 삽입 (idx 뒤로는 한칸씩 밀림)
				result.insert(val_to_idx[remains], "(")
				result.append(")")
				return "".join(result)

			# 나머지로 나온 숫자의 인덱스 기록
			val_to_idx[remains] = len(result)

			# 결과에 remains 추가 후 remains 갱신
			remains *= 10
			result += str(remains // denominator)
			remains = remains % denominator

		return "".join(result)


"""
print(Solution().fractionToDecimal(1, 2))
print(Solution().fractionToDecimal(2, 1))
print(Solution().fractionToDecimal(2, 3))
print(Solution().fractionToDecimal(4, 333))
"""
