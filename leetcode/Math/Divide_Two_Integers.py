class Solution:
	def divide(self, dividend: int, divisor: int) -> int:
		INT_MIN = -pow(2, 31)
		INT_MAX = pow(2, 31) - 1

		# Overflow 발생하는 유일한 케이스
		if dividend == INT_MIN and divisor == -1:
			return INT_MAX

		negative = (dividend > 0 and divisor < 0) or (dividend < 0 and divisor > 0)
		dividend = abs(dividend)
		divisor = abs(divisor)

		low = 0
		high = abs(dividend) + 1

		while low + 1 < high:
			#mid = (low + high) // 2
			mid = low + (high - low) // 2
			if divisor * mid <= dividend:
				low = mid
			else:  # divisor * mid > dividend
				high = mid

		if negative:
			low = -low

		return low


"""
print(Solution().divide(dividend=10, divisor=3))
print(Solution().divide(dividend=7, divisor=-3))
print(Solution().divide(dividend=0, divisor=1))
print(Solution().divide(dividend=1, divisor=1))
print(Solution().divide(dividend=0, divisor=100))
print(Solution().divide(dividend=-pow(2, 31), divisor=-1))
"""
