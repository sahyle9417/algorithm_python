class Solution:
	def mySqrt(self, x: int) -> int:
		# return int(math.sqrt(x))

		if x == 0:
			return 0

		low = 1
		high = x

		while low + 1 < high:
			# python 이외의 언어에서는 overflow 발생 가능하므로 아래와 같이 변형
			# mid = low + (high - low) // 2
			mid = (low + high) // 2
			if mid * mid <= x:
				low = mid
			else:  # mid ** 2 > x
				high = mid

		return low


"""
print(Solution().mySqrt(4))
print(Solution().mySqrt(8))
print(Solution().mySqrt(9))
"""
