class Solution:
	def isRobotBounded(self, instructions: str) -> bool:
		# state = (x, y, direction)
		# north:0, east:1, south:2, west:3
		init_state = (0, 0, 0)
		state = init_state

		dx = [0, 1, 0, -1]
		dy = [1, 0, -1, 0]

		for inst in instructions * 4:

			x, y, dir = state

			if inst == "G":
				x += dx[dir]
				y += dy[dir]
			elif inst == "L":
				dir = (dir + 3) % 4
			else:  # inst == "R"
				dir = (dir + 1) % 4
			
			state = (x, y, dir)

		return state == init_state


print(Solution().isRobotBounded("GGLLGG"))
print(Solution().isRobotBounded("GG"))
print(Solution().isRobotBounded("GL"))
print(Solution().isRobotBounded("GLGLGGLGL"))
