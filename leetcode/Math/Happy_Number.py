class Solution:
	def isHappy(self, n: int) -> bool:
		prev = n
		visited = set()

		while prev != 1:
			curr = self.get_next(prev)

			if curr in visited:
				return False

			visited.add(curr)

			prev = curr

		return True

	def get_next(self, n: int) -> int:
		return sum(map(lambda x: int(x) ** 2, str(n)))


"""
print(Solution().isHappy(19))
print(Solution().isHappy(2))
"""
