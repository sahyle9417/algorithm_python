class Solution:
	def myPow(self, x: float, n: int) -> float:
		if n < 0:
			x = 1 / x
			n = -n

		# x를 제곱하고 n을 2로 나누는 것으로 O(logn) 달성

		answer = 1
		while n > 0:
			if n & 1:  # "if n % 2 == 1"와 동일
				answer = answer * x
			x = x * x
			n >>= 1  # "n = n // 2"와 동일

		return answer


"""
print(Solution().myPow(2, 10))
print(Solution().myPow(2.1, 3))
print(Solution().myPow(2, -2))
print(Solution().myPow(8.84372, -5))
print(pow(8.84372, -5))
"""
