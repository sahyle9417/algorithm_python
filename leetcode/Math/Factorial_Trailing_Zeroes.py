class Solution:
	def trailingZeroes(self, n: int) -> int:
		# trailing zero의 개수는 2와 5가 곱해진 횟수와 같다
		# 단, 2의 개수는 무조건 5의 개수보다 많으므로
		# trailing zero의 개수는 5가 곱해진 횟수와 같다
		# 이때, 5가 여러번 곱해져 있는 경우(25, 125, ...)를 위해
		# n을 5로 반복적으로 나눠서 나온 몫을 결과에 합산한다
		num_fives = 0
		while n:
			n //= 5
			num_fives += n
		return num_fives
