from typing import List
from itertools import product


class Solution:

	def letterCombinations(self, digits: str) -> List[str]:
		if len(digits) == 0:
			return []

		digit_to_letters = {
			"2": "abc",
			"3": "def",
			"4": "ghi",
			"5": "jkl",
			"6": "mno",
			"7": "pqrs",
			"8": "tuv",
			"9": "wxyz",
		}

		# letters : ["abc", "def", "ghi"]
		letters = [digit_to_letters[digit] for digit in digits]
		result = ["".join(p) for p in product(*letters)]

		return result


#print(Solution().letterCombinations("234"))
