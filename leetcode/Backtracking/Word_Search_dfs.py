diff_r = [0, 0, -1, 1]
diff_c = [-1, 1, 0, 0]


class Solution:
	def exist(self, board, word: str) -> bool:
		row_num, col_num = len(board), len(board[0])

		def dfs(row, col, word):

			if len(word) == 0:
				return True

			backup = board[row][col]
			board[row][col] = "#"

			for dr, dc in zip(diff_r, diff_c):
				r = row + dr
				c = col + dc
				if (
					0 <= r < row_num
					and 0 <= c < col_num
					and board[r][c] == word[0]
					and dfs(r, c, word[1:])
				):
					return True

			board[row][col] = backup

		for row in range(row_num):
			for col in range(col_num):

				if board[row][col] == word[0] and dfs(row, col, word[1:]):
					return True

		return False
