from collections import deque
from typing import List


class Solution:
	def subsets(self, nums: List[int]) -> List[List[int]]:
		q = deque()
		result = []

		# state : [done, yet]
		q.append([[], nums])

		while q:
			done, yet = q.popleft()
			result.append(done)

			"""for num in yet:
				yet = yet - {num}
				q.append([done + [num], yet])"""

			for i, num in enumerate(yet):
				q.append([done + [num], yet[i+1:]])

		return result


#print(Solution().subsets([0]))
#print(Solution().subsets([1, 2, 3]))
