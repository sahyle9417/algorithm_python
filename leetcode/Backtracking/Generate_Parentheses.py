from collections import deque
from typing import List


class Solution:
	def generateParenthesis(self, n: int) -> List[str]:
		q = deque()

		# state : (paren, total_open, curr_open)
		init_state = ["", 0, 0]
		q.append(init_state)

		while q:
			paren, total_open, curr_open = q.popleft()

			# 종료 조건 도달
			if len(paren) == n * 2:
				# 종료 조건 검사하느라 하나 pop한 것 도로 append
				q.append([paren, total_open, curr_open])
				break

			# close 괄호 추가
			if curr_open > 0:
				close_state = [paren + ")", total_open, curr_open - 1]
				q.append(close_state)

			# open 괄호 추가
			if total_open < n:
				open_state = [paren + "(", total_open + 1, curr_open + 1]
				q.append(open_state)

		result = [state for state, _, _ in q]
		return result


#print(Solution().generateParenthesis(3))
