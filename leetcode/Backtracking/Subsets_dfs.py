from collections import deque
from typing import List


class Solution:
	def subsets(self, nums: List[int]) -> List[List[int]]:
		ret = []
		self.dfs(done=[], yet=nums, ret=ret)
		return ret

	def dfs(self, done, yet, ret):
		ret.append(done)
		for i, num in enumerate(yet):
			self.dfs(done + [num], yet[i+1:], ret)


#print(Solution().subsets([0]))
#print(Solution().subsets([1, 2, 3]))
