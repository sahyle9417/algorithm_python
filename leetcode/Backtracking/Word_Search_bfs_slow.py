from collections import deque
from typing import List


diff_r = [0, 0, -1, 1]
diff_c = [-1, 1, 0, 0]


class Solution:
	def exist(self, board: List[List[str]], word: str) -> bool:

		row_num = len(board)
		col_num = len(board[0])
		word_len = len(word)

		for row in range(row_num):
			for col in range(col_num):

				if board[row][col] != word[0]:
					continue

				pos = (row, col)
				path = {pos}
				q = deque([[pos, path]])

				match_len = 0

				while q:

					match_len += 1
					q_len = len(q)
					for _ in range(q_len):

						pos, path = q.popleft()

						if match_len == word_len:
							return True

						r, c = pos
						for dr, dc in zip(diff_r, diff_c):
							nr = r + dr
							nc = c + dc

							if nr < 0 or nr >= row_num or nc < 0 or nc >= col_num:
								continue

							if board[nr][nc] != word[match_len]:
								continue

							if (nr, nc) in path:
								continue

							q.append([(nr, nc), path | {(nr, nc)}])

		return False


"""
board = [
	["A", "B", "C", "E"],
	["S", "F", "E", "S"],
	["A", "D", "E", "E"]
]
word = "ABCEFSADEESE"
board = [
	["A", "B", "C", "E"], 
	["S", "F", "C", "S"], 
	["A", "D", "E", "E"]
]
word = "ABCCED"
board = [
	["A", "B", "C", "E"], 
	["S", "F", "C", "S"], 
	["A", "D", "E", "E"]
]
word = "ABCB"
board = [["a", "a"]]
word = "aaa"
board = [
	["A", "B", "C", "E"],
	["S", "F", "E", "S"],
	["A", "D", "E", "E"]
]
word = "ABCESEEEFS"
print(Solution().exist(board, word))
"""
