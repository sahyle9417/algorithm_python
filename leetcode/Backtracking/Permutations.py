from collections import deque
from itertools import permutations
from typing import List


class Solution:
	def permute(self, nums: List[int]) -> List[List[int]]:
		q = deque()

		# state : [done, yet]
		init_state = [[], set(nums)]
		q.append(init_state)

		while q:
			done, yet = q.popleft()
			if len(yet) == 0:
				q.append([done, yet])
				break

			for num in yet:
				q.append([done + [num], yet - {num}])

		result = [done for done, _ in q]
		return result

	# def permute(self, nums: List[int]) -> List[List[int]]:
	#	 return list(permutations(nums))


#print(Solution().permute([1, 2, 3]))
