from typing import List


class Solution:
	def subsets(self, nums: List[int]) -> List[List[int]]:
		done = [[]]
		for new in nums:
			done += [old + [new] for old in done]
		return done
