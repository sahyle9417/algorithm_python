from collections import deque
from typing import Deque, List, Tuple


# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None


class Codec:

	# Encodes a tree to a single string.
	def serialize(self, root: TreeNode) -> str:
		if not root:
			return ""

		node_list = []

		q = deque([root])

		while q:
			node = q.popleft()

			if node is None:
				node_list.append("null")
				continue

			node_list.append(str(node.val))
			q.append(node.left)
			q.append(node.right)

		return ",".join(node_list)

	# Decodes your encoded data to tree.
	def deserialize(self, data: str) -> TreeNode:
		if data == "":
			return None

		# value 담은 string 리스트
		node_list = deque(data.split(","))

		# node 담은 bfs용 큐
		root = TreeNode(int(node_list.popleft()))
		q = deque([root])

		while q:
			node = q.popleft()

			for direction in ["left", "right"]:

				child_val = node_list.popleft()
				if child_val == "null":
					continue

				child = TreeNode(int(child_val))
				if direction == "left":
					node.left = child
				else:
					node.right = child

				q.append(child)

		return root


"""
def build_tree(node_string: str) -> TreeNode:
	if node_string == "":
		return None

	node_list = deque(node_string.split(","))

	node_val = node_list.popleft()
	if node_val == "null":
		return None

	node = TreeNode(int(node_val))
	node.left = build_tree(node_list)
	node.right = build_tree(node_list)

	return node


# Your Codec object will be instantiated and called as such:
ser = Codec()
deser = Codec()
# ans = deser.deserialize(ser.serialize(root))

root = build_tree("")
print(deser.deserialize(ser.serialize(root)))

root = build_tree("3,2,4,3")
print(deser.deserialize(ser.serialize(root)))

root = build_tree("1,2,3,null,null,4,5")
print(deser.deserialize(ser.serialize(root)))
"""
