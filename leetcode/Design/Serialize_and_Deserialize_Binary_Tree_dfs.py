from collections import deque
from typing import Deque, List, Tuple


# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None


class Codec:

	# Encodes a tree to a single string.
	def serialize(self, root: TreeNode) -> str:
		if root is None:
			return ""

		# 조상들을 기억하기 위한 스택
		stack = []

		# 결과를 반환하기 위한 string 리스트
		node_list = []

		node = root

		while node or stack:
			# node가 None이 아니면
			# 해당 노드를 조상스택과 결과리스트에 추가 후 왼쪽 자식으로 이동
			if node:
				node_list.append(str(node.val))
				stack.append(node)
				node = node.left
			# node가 None인 경우 (단, 스택은 비어있지 않음)
			# 결과리스트에 null 추가 후 부모의 우측 자식(내 기준 우측 형제)으로 이동
			else:
				node_list.append("null")
				node = stack.pop()
				node = node.right

		return ",".join(node_list)

	# Decodes your encoded data to tree.
	def deserialize(self, data: str) -> TreeNode:
		if data == "":
			return None

		node_list = data.split(",")

		root = TreeNode(int(node_list[0]))
		node = root

		# 조상들을 기억하기 위한 스택
		stack = []

		for child_val in node_list[1:]:
			child = None
			if child_val == "null":
				child = None
			else:
				child = TreeNode(int(child_val))

			# 자식을 연결받을 부모가 있다면 부모의 왼쪽 자식에 추가
			if node:
				node.left = child
				stack.append(node)
				node = node.left
			# 자식을 연결받을 부모가 없다면 스택에서 부모 가져와서 해당 부모의 우측에 자식 추가
			else:
				node = stack.pop()
				node.right = child
				node = node.right

		return root


"""
def build_tree(node_string: str) -> TreeNode:
	if node_string == "":
		return None

	node_list = deque(node_string.split(","))

	node_val = node_list.popleft()
	if node_val == "null":
		return None

	node = TreeNode(int(node_val))
	node.left = build_tree(node_list)
	node.right = build_tree(node_list)

	return node


# Your Codec object will be instantiated and called as such:
ser = Codec()
deser = Codec()
# ans = deser.deserialize(ser.serialize(root))

root = build_tree("")
print(deser.deserialize(ser.serialize(root)))

root = build_tree("3,2,4,3")
print(deser.deserialize(ser.serialize(root)))

root = build_tree("1,2,3,null,null,4,5")
print(deser.deserialize(ser.serialize(root)))
"""
