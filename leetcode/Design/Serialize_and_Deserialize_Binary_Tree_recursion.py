from collections import deque
from typing import Deque, List, Tuple


# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None


class Codec:

	# Encodes a tree to a single string.
	def serialize(self, root: TreeNode) -> str:
		if root is None:
			return ""
		node_strings = []

		def preorder(node: TreeNode) -> None:
			if not node:
				node_strings.append("null")
			else:
				node_strings.append(str(node.val))
				preorder(node.left)
				preorder(node.right)

		preorder(root)
		return ",".join(node_strings)

	# Decodes your encoded data to tree.
	def deserialize(self, data: str) -> TreeNode:
		if data == "":
			return None

		node_list = deque(data.split(","))

		def build_tree(node_list: Deque) -> TreeNode:
			if len(node_list) == 0:
				return None
			node_val = node_list.popleft()
			if node_val == "null":
				return None

			node = TreeNode(int(node_val))
			node.left = build_tree(node_list)
			node.right = build_tree(node_list)

			return node

		return build_tree(node_list)


"""
def build_tree(node_string: str) -> TreeNode:
	if node_string == "":
		return None

	node_list = deque(node_string.split(","))

	node_val = node_list.popleft()
	if node_val == "null":
		return None

	node = TreeNode(int(node_val))
	node.left = build_tree(node_list)
	node.right = build_tree(node_list)

	return node


# Your Codec object will be instantiated and called as such:
ser = Codec()
deser = Codec()
# ans = deser.deserialize(ser.serialize(root))

root = build_tree("")
print(deser.deserialize(ser.serialize(root)))

root = build_tree("3,2,4,3")
print(deser.deserialize(ser.serialize(root)))

root = build_tree("1,2,3,null,null,4,5")
print(deser.deserialize(ser.serialize(root)))
"""
