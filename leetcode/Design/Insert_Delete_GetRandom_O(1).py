import random


class RandomizedSet:

	# Initialize your data structure here.
	def __init__(self):
		self.val_list = []
		self.val_to_idx = dict()

	# Inserts a value to the set. Returns true if the set did not already contain the specified element.
	def insert(self, val: int) -> bool:
		if val not in self.val_to_idx:
			self.val_list.append(val)
			self.val_to_idx[val] = len(self.val_list) - 1
			return True
		else:
			return False

	# Removes a value from the set. Returns true if the set contained the specified element.
	def remove(self, val: int) -> bool:
		if val in self.val_to_idx:
			empty_idx = self.val_to_idx[val]
			last_val = self.val_list[-1]

			# val이 삭제되어 빈 자리(empty_idx)에 마지막 element(last_val)를 기록
			self.val_list[empty_idx] = last_val
			self.val_to_idx[last_val] = empty_idx

			# 맨 마지막 인덱스 삭제
			self.val_to_idx.pop(val)
			self.val_list.pop()

			return True

		else:
			return False

	# Get a random element from the set.
	def getRandom(self) -> int:
		return random.choice(self.val_list)


# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
