from collections import Counter


def isValid(s):
	char_ctr = Counter(s)
	freq_ctr = Counter(char_ctr.values())

	min_freq = min(freq_ctr)
	max_freq = max(freq_ctr)

	# already valid before removing
	if min_freq == max_freq:
		return "YES"
	# remove max_freq element
	if min_freq + 1 == max_freq and freq_ctr[max_freq] == 1:
		return "YES"
	# remove min_freq element
	if min_freq == 1 and freq_ctr[min_freq] == 1 and len(freq_ctr) == 2:
		return "YES"
	# invalid
	return "NO"


# print(isValid("abcdefghhgfedecba"))
# print(isValid("aabbcd"))
# print(isValid("aabbccddeefghi"))
# print(isValid("aabbccd"))
