from collections import Counter


def sockMerchant(n, ar):
	pair_num = 0

	freqs = Counter(ar).values()
	for freq in freqs:
		pair_num += freq // 2
	
	return pair_num


# print(sockMerchant(n=9, ar=[10, 20, 20, 10, 10, 30, 50, 10, 20]))
