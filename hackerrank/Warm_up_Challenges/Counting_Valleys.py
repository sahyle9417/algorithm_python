def countingValleys(steps, path):
	valley_num = 0
	altitude = 0

	for move in path:
		if move == "U":
			altitude += 1
			if altitude == 0:
				valley_num += 1
		else:
			altitude -= 1

	return valley_num


# print(countingValleys(steps=8, path="UDDDUDUU"))  # 1
# print(countingValleys(steps=12, path="DDUUDDUDUUUD"))  # 2
