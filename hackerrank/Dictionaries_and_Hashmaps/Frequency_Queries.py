from collections import defaultdict


def freqQuery(queries):
	freq_of_val = defaultdict(int)
	freq_of_freq = defaultdict(int)
	answer = []

	for oper, val in queries:

		if oper == 1:
			freq = freq_of_val[val]
			freq_of_val[val] = freq + 1
			freq_of_freq[freq] -= 1
			freq_of_freq[freq + 1] += 1

		elif oper == 2:
			if freq_of_val[val] > 0:
				freq = freq_of_val[val]
				freq_of_val[val] = freq - 1
				freq_of_freq[freq] -= 1
				freq_of_freq[freq - 1] += 1

		else:  # oper == 3
			answer.append(int(freq_of_freq[val] > 0))
	
	return answer


"""
print(freqQuery([
	(1, 5),
	(1, 6),
	(3, 2),
	(1, 10),
	(1, 10),
	(1, 6),
	(2, 5),
	(3, 2)
]))
print(freqQuery([
	(3, 4),
	(2, 1003),
	(1, 16),
	(3, 1)
]))
print(freqQuery([
	(1, 3),
	(2, 3),
	(3, 2),
	(1, 4),
	(1, 5),
	(1, 5),
	(1, 4),
	(3, 2),
	(2, 4),
	(3, 2)
]))
"""
