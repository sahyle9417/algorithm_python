from collections import defaultdict


def freqQuery(queries):
	dic = defaultdict(int)
	answer = []

	for oper, val in queries:

		if oper == 1:
			dic[val] += 1

		elif oper == 2:
			if dic[val] > 0:
				dic[val] -= 1

		else:  # oper == 3
			answer.append(int(val in dic.values()))
	
	return answer


"""
print(freqQuery([
	(1, 5),
	(1, 6),
	(3, 2),
	(1, 10),
	(1, 10),
	(1, 6),
	(2, 5),
	(3, 2)
]))
print(freqQuery([
	(3, 4),
	(2, 1003),
	(1, 16),
	(3, 1)
]))
print(freqQuery([
	(1, 3),
	(2, 3),
	(3, 2),
	(1, 4),
	(1, 5),
	(1, 5),
	(1, 4),
	(3, 2),
	(2, 4),
	(3, 2)
]))
"""
