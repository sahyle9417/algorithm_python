from collections import defaultdict


def sherlockAndAnagrams(s):

	length = len(s)
	substr_ctr = defaultdict(int)

	for start in range(length):
		for end in range(start, length):
			substr = str(sorted(s[start : end + 1]))
			substr_ctr[substr] += 1

	freqs = substr_ctr.values()
	return sum(map(n_comb_2, freqs))


def n_comb_2(n):
	return n * (n - 1) // 2


# print(sherlockAndAnagrams("abcd"))
# print(sherlockAndAnagrams("kkkk"))
# print(sherlockAndAnagrams("ifailuhkqq"))
