from collections import defaultdict


def countTriplets(arr, r):
	answer = 0

	# key가 Triplet의 1번째 숫자로 등장한 횟수
	# 즉 key가 등장한 횟수
	first = defaultdict(int)

	# key가 Triplet의 2번째 숫자로 등장한 경우의 수
	# 즉 (key // r)과 key가 순서대로 등장한 경우의 수
	second = defaultdict(int)

	for curr in arr:

		if curr % r == 0:
			prev = curr // r
			# prev가 2번째로 등장한 횟수만큼 유효한 Triplet 발견
			answer += second[prev]
			# prev가 1번째로 등장한 횟수만큼 curr가 2번째로 등장한 경우의 수에 합산
			second[curr] += first[prev]

		first[curr] += 1

	return answer


# print(countTriplets(arr=[1, 2, 2, 4], r=2))
# print(countTriplets(arr=[1, 3, 9, 9, 27, 81], r=3))
# print(countTriplets(arr=[1, 5, 5, 25, 125], r=5))
