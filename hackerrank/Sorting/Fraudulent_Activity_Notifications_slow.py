from statistics import median


def activityNotifications(expenditure, d):
	answer = 0
	length = len(expenditure)

	for idx in range(d, length):
		med = median(expenditure[idx - d : idx])
		today = expenditure[idx]

		if today >= 2 * med:
			answer += 1

	return answer


# print(activityNotifications(expenditure=[10, 20, 30, 40, 50], d=3))
# print(activityNotifications(expenditure=[2, 3, 4, 2, 3, 6, 8, 4, 5], d=5))
