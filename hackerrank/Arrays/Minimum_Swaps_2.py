def minimumSwaps(arr):
	answer = 0

	# 하나 빼고 제자리에 있다면 모두 제자리임
	# 그래서 마지막 하나 빼고 해도 됨
	for idx in range(len(arr) - 1):

		# 위치에 맞지 않는 값 발견
		while idx != arr[idx] - 1:
			# 해당 값에 맞는 위치와 swap
			arr = swap(arr, idx, arr[idx] - 1)
			answer += 1

	return answer


def swap(arr, i, j):
	tmp = arr[i]
	arr[i] = arr[j]
	arr[j] = tmp
	return arr


# print(minimumSwaps(arr=[4, 3, 1, 2]))
# print(minimumSwaps(arr=[2, 3, 4, 1, 5]))
# print(minimumSwaps(arr=[1, 3, 5, 2, 4, 6, 7]))
