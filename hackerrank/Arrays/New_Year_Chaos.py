import math


def minimumBribes(q):
	bribe = 0

	# 최소값 3개 저장
	mins = [math.inf] * 3

	# 맨 뒤부터 거꾸로 순회 (나보다 뒤에 나보다 작은 값 개수 구하려고)
	for num in reversed(q):

		# 나보다 뒤에 나보다 작은 값이 3개 이상 존재
		if num > mins[2]:
			print("Too chaotic")
			return
		# 나보다 뒤에 나보다 작은 값이 2개 존재
		elif num > mins[1]:
			bribe += 2
		# 나보다 뒤에 나보다 작은 값이 1개 존재
		elif num > mins[0]:
			bribe += 1

		# 최소값 3개 갱신
		if num < mins[0]:
			mins = [num, mins[0], mins[1]]
		elif num < mins[1]:
			mins = [mins[0], num, mins[1]]
		elif num < mins[2]:
			mins = [mins[0], mins[1], num]

	print(bribe)


# minimumBribes(q=[2, 1, 5, 3, 4])
# minimumBribes(q=[2, 5, 1, 3, 4])
